package com.xiaomuedu.validator;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class ParamValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateRequiredString("param", "msg", "param必填");
		String param=c.getPara("param");
		if(param.length()>20){
			addError("msg", "长度不能大于20");
		}
	}

	@Override
	protected void handleError(Controller c) {
		c.renderJson();

	}

}
