package com.xiaomuedu.plugin;

import com.jfinal.plugin.IPlugin;

public class XiaomuPlugin implements IPlugin {

	@Override
	public boolean start() {
		System.out.println("XiaomuPlugin start!");
		return true;
	}

	@Override
	public boolean stop() {
		System.out.println("XiaomuPlugin stop!");
		return true;
	}

}
