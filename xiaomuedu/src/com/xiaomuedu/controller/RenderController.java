package com.xiaomuedu.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.render.ContentType;
import com.jfinal.render.JsonRender;
import com.jfinal.render.JspRender;
import com.xiaomuedu.common.model.User;
/**
 * render系列案例
 * @author 小木(909854136)
 *
 */
public class RenderController extends Controller {
	/**
	 * 本案例首页
	 */
	public void index(){
		render("index.jsp");
	}
	/**
	 * 结果 相对路径
	 */
	public void result1(){
		setAttr("msg", "调用RenderController-result1 返回 /render/result.jsp");
		render("result.jsp");
	}
	/**
	 * 结果
	 */
	public void result(){
		setAttr("msg", "调用RenderController-result 返回 /render/test/result.jsp");
		render("/render/test/result.jsp");
	}
	/**
	 * renderJsp用法
	 */
	public void jsp(){
		setAttr("msg", "调用RenderController-jsp 返回 /render/result.jsp");
		renderJsp("result.jsp");
	}
	/**
	 * renderFreeMarker用法
	 */
	public void fm(){
		setAttr("msg", "调用RenderController-fm 返回 /render/result.ftl");
		renderFreeMarker("result.ftl");
	}
	
	/**
	 * 重定向到的地址
	 */
	public void redirectResult(){
		setAttr("msg", "调用RenderController-redirectResult 返回 /render/result.jsp");
		renderJsp("result.jsp");
	}
	/**
	 * 带参数重定向的action地址
	 */
	public void redirectResultwq(){
		setAttr("msg", "调用RenderController-redirectResultwq 返回 /render/result.jsp  withQueryString:param="+getPara("param"));
		renderJsp("result.jsp");
	}
	/**
	 * 普通重定向
	 */
	public void redirect(){
		redirect("/render/redirectResult");
	}
	/**
	 * 带URL参数重定向
	 */
	public void redirectwq(){
		redirect("/render/redirectResultwq",true);
	}
	/**
	 * 重定向到其他域名
	 */
	public void redirecturl(){
		redirect("http://www.xiaomuedu.com");
	}
	/**
	 * 301重定向
	 */
	public void redirect301(){
		redirect301("http://www.xiaomuedu.com");
	}
	/**
	 * 普通转发到的action
	 */
	public void forwardResult(){
		setAttr("msg", "调用RenderController-forwardResult 返回 /render/result.jsp");
		renderJsp("result.jsp");
	}
	/**
	 * 带参数转发到的action
	 */
	public void forwardResultwp(){
		setAttr("msg", "调用RenderController-forwardResultwp 返回 /render/result.jsp withparam:param="+getAttr("param"));
		renderJsp("result.jsp");
	}
	/**
	 * 普通转发
	 */
	public void forward(){
		forwardAction("/render/forwardResult");
	}
	/**
	 * 带参数转发
	 */
	public void forwardwp(){
		setAttr("param", "xiaomuedu");
		forwardAction("/render/forwardResultwp");
	}
	/**
	 * 纯文本
	 */
	public void text(){
		renderText("小木学堂：www.xiaomuedu.com");
	}
	/**
	 * 文本-xml格式
	 */
	public void textxml(){
		renderText("<dept><name>小木学堂</name><url>http://www.xiaomuedu.com</url></dept>","application/xml");
	}
	/**
	 * 文本-html格式
	 */
	public void texthtml(){
		renderText("<center><div style='text-align:center;margin:20px auto;'><h2 style='display:block;margin-bottom:10px;'>小木学堂</h2><a href='http://www.xiaomuedu.com'>http://www.xiaomuedu.com</a></div></center>",ContentType.HTML);
	}
	/**
	 * html格式文本
	 */
	public void html(){
		renderHtml("<center><div style='text-align:center;margin:20px auto;'><h2 style='display:block;margin-bottom:10px;'>小木学堂</h2><a href='http://www.xiaomuedu.com'>http://www.xiaomuedu.com</a></div></center>");
	}
	/**
	 * xml格式文本 静态
	 */
	public void xml(){
		renderXml("renderxml.xml");
	}
	/**
	 * xml格式文本 动态
	 */
	public void xml2(){
		setAttr("name", "小木");
		setAttr("sex", "男");
		setAttr("age", "28");
		setAttr("job", "CTO");
		renderXml("renderxml2.xml");
	}
	/**
	 * xml格式文本 列表
	 */
	public void xml3(){
		List<Record> records=new ArrayList<>();
		records.add(new Record().set("name", "小木").set("sex", "男").set("age", 28).set("job", "CTO"));
		records.add(new Record().set("name", "小木001").set("sex", "女").set("age", 27).set("job", "CEO"));
		records.add(new Record().set("name", "小木002").set("sex", "男").set("age", 29).set("job", "CFO"));
		setAttr("records", records);
		renderXml("renderxml3.xml");
	}
	/**
	 * 进入renderJavascript测试
	 */
	public void testjs(){
		renderJsp("test/testjs.jsp");
	}
	/**
	 * 测试renderJavascript
	 */
	public void js(){
		renderJavascript("function testjs(){alert('远程 testjs 执行');}");
	}
	/**
	 * 404错误信息渲染
	 */
	public void error1(){
		renderError(404);
	}
	/**
	 * 404错误信息渲染 指定view
	 */
	public void error2(){
		renderError(404, "/common/404.html");
	}
	/**
	 * 404错误信息渲染 指定render
	 */
	public void error3(){
		renderError(404,new JspRender("/common/404.html"));
	}
	/**
	 * 404错误信息渲染 指定render
	 */
	public void error4(){
		renderError(404,new JsonRender("msg", "访问RenderControler error4"));
	}
	/**
	 * 对客户端不做任何响应
	 */
	public void none(){
		System.out.println("执行renderNull");
		renderNull();
	}
	/**
	 * 渲染Json格式数据 无参数
	 */
	public void json(){
		List<User> users=User.dao.find("select * from user"); 
		setAttr("users", users);
		setAttr("msg", "调用了renderController中的json方法 renderJson()");
		renderJson();
	}
	/**
	 * 渲染Json格式数据 renderJson(obj)
	 */
	public void jsonobj(){
		List<User> users=User.dao.find("select * from user"); 
		renderJson(users);
	}
	/**
	 * 渲染Json格式数据 renderJson(key,value)
	 */
	public void jsonkv(){
		List<User> users=User.dao.find("select * from user"); 
		renderJson("users",users);
	}
	/**
	 * 渲染Json格式数据 renderJson(obj)
	 */
	public void jsonmap(){
		List<User> users=User.dao.find("select * from user");
		Map<String, Object> map=new HashMap<>();
		map.put("users", users);
		map.put("msg", "调用了renderController中的jsonmap方法 renderJson(map)");
		renderJson(map);
	}
	/**
	 * 渲染Json格式数据 renderJson(jsonText)
	 */
	public void jsontext(){
		List<User> users=User.dao.find("select * from user"); 
		String jsontext=JsonKit.toJson(users);
		renderJson(jsontext);
	}
	/**
	 * 渲染Json格式数据 renderJson(jsonText) 自定义jsontext
	 */
	public void jsontext2(){
		renderJson("{\"msg\":\"调用了RenderController中的jsontext2方法 renderJson(jsontext)\"}");
	}
	
	/**
	 * 渲染Json格式数据 renderJson(attrs)
	 */
	public void jsonattrs(){
		List<User> users=User.dao.find("select * from user"); 
		setAttr("users", users);
		setAttr("msg", "调用了renderController中的jsonattrs方法 renderJson(attrs)");
		renderJson(new String[]{"users"});
	}

}
