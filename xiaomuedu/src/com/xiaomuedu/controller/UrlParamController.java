package com.xiaomuedu.controller;

import com.jfinal.core.Controller;
import com.xiaomuedu.common.model.User;
/**
 * Url传参接参案例
 * @author 小木（909854136）
 *
 */
public class UrlParamController extends Controller {
	public void index(){
		renderJsp("index.jsp");
	}
	/**
	 * url参数 key-value类型
	 */
	public void keyvalue(){
		setAttr("msg", "使用 getPara(\"p1\")得到p1的参数值:getPara(key)");
		setAttr("p1","p1=" +getPara("p1"));
		setAttr("p2","p2=" + getPara("p2"));
		setAttr("p3","p3=" + getPara("p3"));
		renderJsp("result.jsp");
	}
	/**
	 * url参数 无key类型
	 */
	public void nokey(){
		setAttr("msg", "使用 getPara(0)得到p1的参数值:getPara(index)");
		setAttr("p1","p1=" +getPara(0));
		setAttr("p2","p2=" + getPara(1));
		setAttr("p3","p3=" + getPara(2));
		renderJsp("result.jsp");
	}
	/**
	 * url参数 转型 int
	 */
	public void changetypetoint(){
		setAttr("msg", "使用 getParaToInt(0)得到p1的参数值:getParaToInt(index)");
		setAttr("p1","p1=" +getParaToInt(0));
		setAttr("p2","p2=" + getParaToInt(1));
		setAttr("p3","p3=" + getParaToInt(2));
		renderJsp("result.jsp");
	}
	/**
	 * url参数 转型 int
	 */
	public void changetypetoint2(){
		setAttr("msg", "使用 getParaToInt(\"p1\")得到p1的参数值:getParaToInt(key)");
		setAttr("p1","p1=" +getParaToInt("p1"));
		setAttr("p2","p2=" + getParaToInt("p2"));
		setAttr("p3","p3=" + getParaToInt("p3"));
		renderJsp("result.jsp");
	}
	
	/**
	 * url参数 默认值
	 */
	public void defaultvalue(){
		setAttr("msg", "使用 getPara(\"p1\",100)得到p1的参数值:getPara(key,defaultValue)");
		setAttr("p1","p1=" +getPara("p1", "100"));
		setAttr("p2","p2=" + getParaToInt("p2",200));
		setAttr("p3","p3=" + getParaToInt("p3",300));
		renderJsp("result.jsp");
	}
	/**
	 * url参数 model
	 */
	public void model(){
		setAttr("msg", "使用 getModel(User.class,\"user\")得到p1的参数值:getModel(class,key)");
		setAttr("user", getModel(User.class, "user"));
		renderJsp("model.jsp");
	}
	
}
