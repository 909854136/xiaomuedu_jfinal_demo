package com.xiaomuedu.controller;

import com.jfinal.core.Controller;
import com.jfinal.kit.LogKit;
/**
 * AJAX传参与接参
 * @author 小木 909854136
 *
 */
public class AjaxController extends Controller {
	/**
	 * 进入案例首页
	 */
	public void index(){
		renderJsp("index.jsp");
	}
	/**
	 * get方式 ajax请求 
	 */
	public void get(){
		String param=getPara("param");
		setAttr("success", true);
		setAttr("type", "get");
		setAttr("param", param);
		setAttr("msg", "json data");
		renderJson();
	}
	/**
	 * post方式 ajax请求 
	 */
	public void post(){
		String param=getPara("param");
		setAttr("success", true);
		setAttr("type", "post");
		setAttr("param", param);
		setAttr("msg", "json data");
		renderJson();
	}
	/**
	 *  ajax提交form
	 */
	public void form(){
		setAttr("success", true);
		setAttr("type", "post");
		setAttr("p1", getPara("p1"));
		setAttr("p2", getPara("p2"));
		setAttr("p3", getPara("p3"));
		setAttr("msg", "json data");
		renderJson();
	}

}
