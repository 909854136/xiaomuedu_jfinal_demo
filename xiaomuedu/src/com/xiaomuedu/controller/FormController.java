package com.xiaomuedu.controller;

import com.jfinal.core.Controller;
/**
 * form表单传参与接参
 * @author Administrator
 *
 */
public class FormController extends Controller {
	public void index(){
		renderJsp("index.jsp");
	}
	/**
	 * 1、form表单 普通post
	 */
	public void normal(){
		setAttr("msg", "form表单普通post");
		setAttr("p1", getPara("p1"));
		setAttr("p2", getPara("p2"));
		setAttr("p3", getPara("p3"));
		renderJsp("result.jsp");
	}
	/**
	 * 2、form表单 普通get
	 */
	public void get(){
		setAttr("msg", "form表单普通get");
		System.out.println(getPara("p1"));
		setAttr("p1", getPara("p1"));
		setAttr("p2", getPara("p2"));
		setAttr("p3", getPara("p3"));
		renderJsp("result.jsp");
	}
}
