package com.xiaomuedu.controller;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
/**
 * 路由配置测试
 * @author 小木学堂
 *
 */
public class RoutesController extends Controller {
	/**
	 * 路由配置案例页面首页
	 */
	public void index(){
		Integer p1=getParaToInt(0);
		Integer p2=getParaToInt(1);
		if(p1!=null){
			setAttr("p1", "index参数1:"+p1);
		}
		if(p2!=null){
			setAttr("p2", "index参数2:"+p2);
		}
		if(p1!=null){
			setAttr("msg", "访问RoutesController.index()方法");
			render("result.jsp");
		}else{
			render("index.jsp");
		}
	}
	/**
	 * 访问methodname
	 */
	public void method1(){
		setAttr("msg", "访问RoutesController.method1()方法");
		render("result.jsp");
	}
	/**
	 * 访问methodname
	 */
	public void method2(){
		setAttr("msg", "访问RoutesController.method2()方法");
		render("result.jsp");
	}
	/**
	 * 访问方法并且传参数
	 */
	public void methodParam(){
		setAttr("msg", "访问RoutesController.methodParam()方法");
		Integer p1=getParaToInt(0);
		Integer p2=getParaToInt(1);
		if(p1!=null){
			setAttr("p1", "methodParam参数1:"+p1);
		}
		if(p2!=null){
			setAttr("p2", "methodParam参数2:"+p2);
		}
		render("result.jsp");
	}
	/**
	 * 自定义controllerKey
	 */
	@ActionKey("/ak")
	public void actionKey(){
		setAttr("msg", "访问自定义的RoutesController.actionkey()方法");
		Integer p1=getParaToInt(0);
		Integer p2=getParaToInt(1);
		if(p1!=null){
			setAttr("p1", "methodParam参数1:"+p1);
		}
		if(p2!=null){
			setAttr("p2", "methodParam参数2:"+p2);
		}
		render("result.jsp");
	}
}
