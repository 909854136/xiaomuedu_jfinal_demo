package com.xiaomuedu.controller;

import java.util.List;

import com.jfinal.core.Controller;
import com.xiaomuedu.common.model.Area;

public class AreaController extends Controller {
	public void index(){
		renderJsp("index.jsp");
	}
	/**
	 * 读取省
	 */
	public void pro(){
		List<Area> ares=Area.dao.find("select id as optionValue,name as optionText from area where pid=0 and type=1");
		setAttr("success", true);
		setAttr("result", ares);
		renderJson();
	}
	/**
	 * 读取市
	 */
	public void city(){
		List<Area> ares=Area.dao.find("select id as optionValue,name as optionText from area where pid=?",getPara(0));
		setAttr("success", true);
		setAttr("result", ares);
		renderJson();
	}
}
