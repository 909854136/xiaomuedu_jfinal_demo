package com.xiaomuedu.controller;

import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.upload.UploadFile;
/**
 * 使用form上传文件
 * @author 小木(909854136)
 *
 */
public class UploadController extends Controller {
	/**
	 * 案例首页
	 */
	public void index(){
		renderJsp("index.jsp");
	}
	/**
	 * 普通form上传
	 */
	public void formNormal(){
		UploadFile file=getFile("file");
		setAttr("msg", "调用文件上传uploadController:formNormal");
		setAttr("filePath", file.getUploadPath()+"\\"+file.getFileName());
		setAttr("url","xmupload/"+file.getFileName());
		renderJsp("result.jsp");
	}
	/**
	 * form上传 带参数
	 */
	public void formwithparam(){
		UploadFile file=getFile("file");
		String param=getPara("param");
		setAttr("msg", "调用文件上传uploadController:formwithparam 参数值："+param);
		setAttr("filePath", file.getUploadPath()+"\\"+file.getFileName());
		setAttr("url","xmupload/"+file.getFileName());
		renderJsp("result.jsp");
	}
	
	/**
	 * ajax form上传
	 */
	public void ajaxForm4(){
		UploadFile file=getFile("file");
		setAttr("msg", "调用文件上传uploadController:ajaxForm4");
		setAttr("filePath", file.getUploadPath()+"\\"+file.getFileName());
		setAttr("url","xmupload/"+file.getFileName());
		renderJson();
	}
	/**
	 * ajax form上传 带参数
	 */
	public void ajaxForm5(){
		UploadFile file=getFile("file");
		String param=getPara("param");
		setAttr("msg", "调用文件上传uploadController:ajaxForm5");
		setAttr("param", param);
		setAttr("filePath", file.getUploadPath()+"\\"+file.getFileName());
		setAttr("url","xmupload/"+file.getFileName());
		renderJson();
	}
	/**
	 * uplodify组件整合
	 */
	public void uplodify(){
		UploadFile file=getFile("Filedata");
		setAttr("msg", "调用文件上传uploadController:uplodify");
		setAttr("filePath", file.getUploadPath()+"\\"+file.getFileName());
		setAttr("url","xmupload/"+file.getFileName());
		renderJson();
	}
	
	
}
