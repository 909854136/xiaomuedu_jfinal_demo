package com.xiaomuedu.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xiaomuedu.bean.UserBean;
import com.xiaomuedu.common.model.Log;
import com.xiaomuedu.common.model.User;
/**
 * Model系列课程案例
 * @author 小木
 *
 */
public class ModelController extends Controller{
	public void index(){
		setAttr("users", User.dao.find("select * from user"));
		renderJsp("index.jsp");
	}
	/**
	 * 自己创建Model实例 set属性值后保存
	 */
	public void setsave(){
		setAttr("msg", "调用ModelController-setsave方法");
		Log log=new Log();
		log.set("content", "调用ModelController-setsave方法");
		log.set("createTime", new Date());
		boolean success=log.save();
		setAttr("success", success);
		setAttr("model", log);
		renderJson();
	}
	/**
	 * 从请求中获取model 保存
	 */
	public void getsave(){
		Log log=getModel(Log.class, "log");
		log.set("createTime", new Date());
		boolean success=log.save();
		setAttr("msg", "调用ModelController-getsave方法");
		setAttr("success", success);
		setAttr("model", log);
		renderJson();
	}
	/**
	 * 从请求中获得Model getModel
	 */
	public void getmodel(){
		setAttr("model", getModel(User.class));
		setAttr("msg", "调用ModelController-getmodel方法");
		renderJson();
	}
	/**
	 * 从请求中获得JavaBean getBean
	 */
	public void getbean(){
		setAttr("bean", getBean(UserBean.class,"user"));
		setAttr("msg", "调用ModelController-getbean方法");
		renderJson();
	}
	
	/**
	 * 根据所有符合条件的
	 */
	public void find(){
		setAttr("users", User.dao.find("select * from user where name like '%小木%'"));
		setAttr("msg", "调用ModelController-find方法");
		renderJson();
	}
	/**
	 * 根据id查询唯一数据 一条结果
	 */
	public void findbyid(){
		setAttr("user", User.dao.findById(getPara(0)));
		setAttr("msg", "调用ModelController-findbyid方法");
		renderJson();
	}
	/**
	 * 根据根据id查询唯一数据 一条结果 但是被过滤了 只查询返回指定字段列
	 */
	public void findByIdLoadColumns(){
		setAttr("user", User.dao.findByIdLoadColumns(getPara("id"), getPara("columns")));
		setAttr("msg", "调用ModelController-findByIdLoadColumns方法");
		renderJson();
	}
	/**
	 * 根据根据条件查询第一个
	 */
	public void findfirst(){
		setAttr("user", User.dao.findFirst("select * from user"));
		setAttr("msg", "调用ModelController-findfirst方法");
		renderJson();
	}
	/**
	 * 根据根据条件查询第一个
	 */
	public void findfirst2(){
		setAttr("user", User.dao.findFirst("select * from user where name=?","小木"));
		setAttr("msg", "调用ModelController-findfirst2方法");
		renderJson();
	}
	
	/**
	 * 普通set修改
	 */
	public void setupdate1(){
		Integer id=getParaToInt("id");
		String name=getPara("name");
		User user=new User();
		user.set("id", id);
		user.set("name", name);
		boolean success=user.update();
		setAttr("success", success);
		setAttr("user", User.dao.findById(id));
		setAttr("msg", "调用ModelController-setupdate1方法");
		renderJson();
	}
	/**
	 * 根据id先get 后set修改
	 */
	public void setupdate2(){
		Integer id=getParaToInt("id");
		String name=getPara("name");
		User user=User.dao.findById(id);
		if(user==null){
			setAttr("errorMsg", "id 无效");
		}else{
			user.set("name", name);
			boolean success=user.update();
			setAttr("success", success);
		}
		setAttr("user", User.dao.findById(id));
		setAttr("msg", "调用ModelController-setupdate2方法");
		renderJson();
	}
	
	/**
	 * 普通get修改
	 */
	public void getupdate(){
		User user=getModel(User.class,"user");
		if(user.getId()==null||user.getId()<=0){
			setAttr("errorMsg", "id无效");
		}else{
			boolean success=user.update();
			setAttr("success", success);
		}
		setAttr("user", User.dao.findById(user.getId()));
		setAttr("msg", "调用ModelController-setupdate方法");
		renderJson();
	}
	/**
	 * set-delete
	 */
	public void setdelete(){
		Integer id=getParaToInt(0);
		User user=new User();
		user.setId(id);
		boolean success=user.delete();
		setAttr("success", success);
		User dbUser=User.dao.findById(id);
		setAttr("user", dbUser==null?"已经被删除":"删除失败");
		setAttr("msg", "调用ModelController-setdelete方法");
		renderJson();
	}
	/**
	 * get-delete
	 */
	public void getdelete(){
		Integer id=getParaToInt(0);
		User user=User.dao.findById(id);
		if(user==null){
			setAttr("errormsg", "id 无效");
		}else{
			boolean success=user.delete();
			setAttr("success", success);
		}
		User dbUser=User.dao.findById(id);
		setAttr("user", dbUser==null?"已经被删除":"删除失败");
		setAttr("msg", "调用ModelController-getdelete方法");
		renderJson();
	}
	/**
	 * delbyid
	 */
	public void delbyid(){
		Integer id=getParaToInt(0);
		boolean success=User.dao.deleteById(id);
		setAttr("success", success);
		User dbUser=User.dao.findById(id);
		setAttr("user", dbUser==null?"已经被删除":"删除失败");
		setAttr("msg", "调用ModelController-delbyid方法");
		renderJson();
	}
	/**
     * 获取前端传来的数组对象并响应成Model列表
     *
     * @param modelClass
     * @param modelName
     * @return
     */
    public <T> List<T> getModels(Class<T> modelClass, String modelName) {
        List<String> nos = getModelsNoList(modelName);
        List<T> list =new ArrayList<T>();
        for (String no : nos) {
            T m = getModel(modelClass, modelName + "[" + no + "]");
            if (m != null) {
                list.add(m);
            }
        }
        return list;
    }

    /**
     * 提取model对象数组的标号
     *
     * @param modelName
     * @return
     */
    protected List<String> getModelsNoList(String modelName) {
        // 提取下标
        List<String> list = new ArrayList<String>();
        String modelNameAndLeft = modelName + "[";
        Map<String, String[]> parasMap = getRequest().getParameterMap();
        for (Map.Entry<String, String[]> e : parasMap.entrySet()) {
            String paraKey = e.getKey();
            if (paraKey.startsWith(modelNameAndLeft)) {
                String no = paraKey.substring(paraKey.indexOf("[") + 1,
                        paraKey.indexOf("]"));
                if (!list.contains(no)) {
                    list.add(no);
                }
            }
        }
        Collections.sort(list);
        return list;
    }
	/**
	 * 获得多个model - save
	 */
	public void getmodelssave(){
		setAttr("olgCount",Db.queryLong("select count(*) from user"));
		//第一种
//		List<User> users=getModels(User.class,"user");
		//for(User user:users){
			//user.set("sex", 1);
		//}
//		setAttr("得到users",users);
//		Db.batchSave(users, users.size());
		//第二种
		String[] names=getParaValues("user.name");
		Integer[] ages=getParaValuesToInt("user.age");
		int len=names.length;
		Object[][] paras=new Object[len][3];
		for(int i=0;i<len;i++){
			paras[i][0]=names[i];
			paras[i][1]=ages[i];
			paras[i][2]=1;
		}
		Db.batch("insert into user(name,age,sex) values(?,?,?)", paras, len);
		
		setAttr("count",Db.queryLong("select count(*) from user"));
		setAttr("msg", "调用ModelController-getmodelssave方法");
		renderJson();
	}
	/**
	 * 批量获取与更新
	 */
	public void getmodelsupdate(){
		//第一种
		/*String[] ids=getParaValues("user.id");
		String[] names=getParaValues("user.name");
		Integer[] ages=getParaValuesToInt("user.age");
		int len=ids.length;
		Object[][] paras=new Object[len][3];
		for(int i=0;i<len;i++){
			paras[i][0]=ages[i];
			paras[i][1]=names[i];
			paras[i][2]=ids[i];
		}
		Db.batch("update user set age=?,name=? where id=?", paras, len);*/
		//第二种
		List<User> users=getModels(User.class,"user");
		Db.batchUpdate(users, users.size());
		setAttr("users",User.dao.find("select id,name,age from user"));
		setAttr("msg", "调用ModelController-getmodelsupdate方法");
		renderJson();
	}
	/**
	 * 所有男变女
	 */
	public void updatemodels1(){
		Db.update("update user set sex=2 where sex=1");
		
		setAttr("users",User.dao.find("select * from user"));
		setAttr("msg", "调用ModelController-updatemodels1方法");
		renderJson();
	}
	/**
	 * 所有女变男
	 */
	public void updatemodels4(){
		Db.update("update user set sex=1 where sex=2");
		
		setAttr("users",User.dao.find("select * from user"));
		setAttr("msg", "调用ModelController-updatemodels4方法");
		renderJson();
	}
	/**
	 * 更新所有女年龄21
	 */
	public void updatemodels2(){
		Db.update("update user set age=21 where sex=2");
		setAttr("users",User.dao.find("select * from user"));
		setAttr("msg", "调用ModelController-updatemodels2方法");
		renderJson();
	}
	/**
	 * 更新所有名字带小木的年龄28
	 */
	public void updatemodels3(){
		Db.update("update user set age=28 where name like '%小木%'");
		setAttr("users",User.dao.find("select * from user"));
		setAttr("msg", "调用ModelController-updatemodels3方法");
		renderJson();
	}
	/**
	 * 多ID 批量删除
	 */
	public void batchdel(){
		Integer ids[]=getParaValuesToInt("id");
		if(ids==null||ids.length==0){
			setAttr("msg", "请选择要删除的数据");
		}else{
			//第一种
			/*Object[][] paras=new Object[ids.length][1];
			for(int i=0;i<ids.length;i++){
				paras[i][0]=ids[i];
			}
			Db.batch("delete from user where id=?",paras,ids.length);*/
			//第二种
//			for(int i=0;i<ids.length;i++){
//				User.dao.deleteById(ids[i]);
//				//Db.update("delete from user where id=?", ids[i]);
//			}
/*			//第三种
			String str=new String();
			for(int i=0;i<ids.length;i++){
				str+=ids[i];
				if(i<ids.length-1){
					str+=",";
				}
			}
			Db.update("delete from user where id in ("+str+")");
*/			//第四种
			String str=new String();
			for(int i=0;i<ids.length;i++){
				str+="?";
				if(i<ids.length-1){
					str+=",";
				}
			}
			Db.update("delete from user where id in ("+str+")",ids);
			setAttr("users",User.dao.find("select * from user"));
			setAttr("msg", "调用ModelController-batchdel方法");
		}
		renderJson();
	}
	/**
	 * 按条件批量删除 删除 小于18岁的女生
	 */
	public void batchdel2(){
		Db.update("delete from user where age<18 and sex=2");
		setAttr("users",User.dao.find("select * from user"));
		setAttr("msg", "调用ModelController-batchdel2方法");
		renderJson();
	}
	/**
	 * 删除所有名字带小木
	 */
	public void batchdel3(){
		Db.update("delete from user where name like '%小木%'");
		setAttr("users",User.dao.find("select * from user"));
		setAttr("msg", "调用ModelController-batchdel3方法");
		renderJson();
	}
}
