package com.xiaomuedu.controller;

import java.util.List;

import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.upload.UploadFile;
import com.xiaomuedu.common.model.User;
/**
 * 用户CURD
 * @author 小木学堂
 *
 */
@Clear
public class UserController extends Controller {
	/**
	 * 直接访问user地址进入list.jsp
	 */
	public void index(){
		List<User> users=User.dao.find("select * from user");
		setAttr("users", users);
		System.out.println("得到数据"+users.size()+"个");
		render("list.jsp");
	}
	/**
	 * 访问user/form 地址进入form.jsp
	 */
	public void form(){
		Integer id=getParaToInt(0);
		if(id!=null&&id>0){
			setAttr("user", User.dao.findById(id));
		}
		render("form.jsp");
	}
	/**
	 * 数据提交
	 */
	public void submit(){
		User user=getModel(User.class,"user");
		user.save();
		redirect("/user");
	}
	public void update(){
		User user=getModel(User.class,"user");
		user.update();
		redirect("/user");
	}
	public void search(){
		List<User> users=User.dao.find("select * from user where name like '%"+getPara("keywords")+"%' or remark like '%"+getPara("keywords")+"%'");
		setAttr("users", users);
		keepPara();
		System.out.println("得到数据"+users.size()+"个");
		render("list.jsp");
	}
	public void edit(){
		form();
	}
	public void del(){
		User.dao.deleteById(getPara(0));
		redirect("/user");
	}
	
	public void upload(){
		UploadFile file=getFile("img");
		renderHtml("<strong>上传成功</strong><img src='"+JFinal.me().getConstants().getBaseUploadPath()+"/"+file.getFileName()+"'/>");
	}
	
	public void download(){
		renderFile("1.txt");
	}
	
	public void renderftl(){
		setAttr("msg", "hello Freemarker");
		render("user");
	}
	
	public void testUrlParaSeparator(){
		setAttr("param1", getPara(0));
		setAttr("param2", getPara(1));
		render("param.jsp");
	}
}
