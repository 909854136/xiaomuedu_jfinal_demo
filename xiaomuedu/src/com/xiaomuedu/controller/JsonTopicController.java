package com.xiaomuedu.controller;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.xiaomuedu.common.model.User;
/**
 * JSON专题
 * @author 小木
 *
 */
public class JsonTopicController extends Controller {
	public void index(){
		renderJsp("index.jsp");
	}
	
	public void test1(){
		List<User> users=User.dao.find("select * from user"); 
		setAttr("users", users);
		renderJson();
	}
	
}
