package com.xiaomuedu.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.xiaomuedu.validator.ParamValidator;
/**
 * 数据校验
 * @author 小木
 *
 */
public class ParamValidatorController extends Controller {
	public void index(){
		renderJsp("index.jsp");
	}
	@Before(ParamValidator.class)
	public void submit(){
		setAttr("msg", "校验成功 执行submit");
		renderJson();
	}
}
