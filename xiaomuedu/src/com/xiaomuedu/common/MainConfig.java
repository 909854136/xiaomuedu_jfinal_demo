package com.xiaomuedu.common;

import java.util.Date;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.FakeStaticHandler;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.xiaomuedu.common.model.Area;
import com.xiaomuedu.common.model.Log;
import com.xiaomuedu.common.model._MappingKit;
import com.xiaomuedu.controller.RoutestestController;
import com.xiaomuedu.handler.LogHandler;
import com.xiaomuedu.interceptor.GlobalInterceptor;
import com.xiaomuedu.plugin.XiaomuPlugin;

public class MainConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		//JFinal常量配置讲解
		//常量配置是第一个被加载的因为后面的一些配置都需要这里配置的常量作为基础
		// 1.加载数据库配置 读取配置文件使用loadPropertyFile或者PropKit  读取键值对
		//loadPropertyFile("config.properties");
		PropKit.use("config.properties");
		// 2、设置开发模式
		me.setDevMode(PropKit.getBoolean("devMode"));
//		me.setDevMode(getPropertyToBoolean("devMode"));
		//设置Action Report什么出现 默认true
		me.setReportAfterInvocation(false);
		// 3、配置默认的视图类型 默认是Freemarker
		me.setViewType(ViewType.JSP);
		// 4、配置默认视图层路径viewpath
		//me.setBaseViewPath("/WEB-INF/view");
		// 5、设置默认上传路径 cos组件有效 jfinal默认有值 相对 绝对都可以
		me.setBaseUploadPath("xmupload");
		//me.setMaxPostSize(1024*1024*20);
		// 6、设置默认下载路径 cos组件有效 jfinal默认有值  相对 绝对都可以
		me.setBaseDownloadPath("xmdownload");
		// 7、设置默认的Freemarker模板文件后缀名 jfinal默认.html
		//me.setFreeMarkerViewExtension(".ftl");
		//me.setJspViewExtension(".jtl");
		//me.setVelocityViewExtension(".vtl");
		// 8、这是url参数分隔符  默认-
		//me.setUrlParaSeparator("~");
		//设置国际化
		//me.setI18nDefaultBaseName("i18n");
		//me.setI18nDefaultLocale("zh_CN"");
		//设置Error View
		//me.setError404View("/common/404.html");
		//me.setErrorRenderFactory(errorRenderFactory);
		//设置默认编码
		//me.setEncoding("GBK");
		//设置默认的xml渲染工厂 默认使用Freemarker render渲染
		//me.setXmlRenderFactory(自定义工厂);
		//设置默认json中时间格式化
		me.setJsonDatePattern("yyyy-MM-dd HH:mm");
		me.setJsonFactory(FastJsonFactory.me());
		//renderJson 和JsonKit底层依赖于JsonManager中设置的JsonFactory
		//设置自己的Log工厂实现
		//me.setLogFactory(Slf4JLogFactory.me());
		
	}

	@Override
	public void configRoute(Routes me) {
		me.add(new FrontRoutes());
		me.add(new AdminRoutes());
//		//根路径访问映射
//		me.add("/", IndexController.class);
//		//其他
//		me.add("/user", UserController.class);
//		//路由配置测试案例专用
//		me.add("/routes", RoutesController.class);
		//自定义viewpath
		//me.add("/routestest", RoutestestController.class);
		me.add("/routestest", RoutestestController.class,"/myroutes/");
	}

	@Override
	public void configPlugin(Plugins me) {
		//Jfinal配置插件
		//数据库连接池
//		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password"));
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password"));
		//ORM Activerecord
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setShowSql(true);
		arp.addMapping("area", Area.class);
		_MappingKit.mapping(arp);
		XiaomuPlugin xm=new XiaomuPlugin();
		me.add(xm);
		
		//加入插件管理器
		me.add(c3p0Plugin);
		me.add(arp);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// JFinal配置拦截器
		//me.add(new GlobalInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		// JFinal配置处理器
		//me.add(new LogHandler());
		//me.add(new FakeStaticHandler(".xm"));
	}
	@Override
	public void afterJFinalStart() {
		Log log=new Log();
		log.set("content", "系统启动之后回调afterJfinalStart");
		log.set("createTime", new Date());
		log.save();
		System.out.println("系统启动之后回调afterJfinalStart");
	}
	@Override
	public void beforeJFinalStop() {
		Log log=new Log();
		log.set("content", "系统关闭之前回调beforeJFinalStop");
		log.set("createTime", new Date());
		log.save();
		
		System.out.println("系统关闭之前回调beforeJFinalStop");
	}
	public static void main(String[] args) {
		JFinal.start("WebRoot", 80, "/", 5);
	}

}
