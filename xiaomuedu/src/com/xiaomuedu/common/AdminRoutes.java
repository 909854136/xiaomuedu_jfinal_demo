package com.xiaomuedu.common;

import com.jfinal.config.Routes;
import com.xiaomuedu.controller.RoutesController;

public class AdminRoutes extends Routes {

	@Override
	public void config() {
		this.add("/routes", RoutesController.class);
	}

}
