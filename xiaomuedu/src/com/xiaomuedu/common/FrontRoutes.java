package com.xiaomuedu.common;

import com.jfinal.config.Routes;
import com.xiaomuedu.controller.AjaxController;
import com.xiaomuedu.controller.AreaController;
import com.xiaomuedu.controller.CaptchaController;
import com.xiaomuedu.controller.EmployeeController;
import com.xiaomuedu.controller.FormController;
import com.xiaomuedu.controller.GlobalInterceptorController;
import com.xiaomuedu.controller.IndexController;
import com.xiaomuedu.controller.JsonTopicController;
import com.xiaomuedu.controller.ModelController;
import com.xiaomuedu.controller.ParamValidatorController;
import com.xiaomuedu.controller.RecordController;
import com.xiaomuedu.controller.RenderController;
import com.xiaomuedu.controller.UploadController;
import com.xiaomuedu.controller.UrlParamController;
import com.xiaomuedu.controller.UserController;
public class FrontRoutes extends Routes {

	@Override
	public void config() {
		this.add("/", IndexController.class);
		this.add("/user", UserController.class);
		this.add("/urlparam", UrlParamController.class);
		this.add("/form", FormController.class);
		this.add("/ajax", AjaxController.class);
		this.add("/upload", UploadController.class,"/form/upload");
		this.add("/render", RenderController.class);
		this.add("/model", ModelController.class);
		this.add("/record", RecordController.class);
		this.add("/area", AreaController.class);
		this.add("/employee", EmployeeController.class);
		this.add("/pv", ParamValidatorController.class);
		this.add("/jsontopic", JsonTopicController.class);
		this.add("/captcha", CaptchaController.class);
		this.add("/globalInterceptor", GlobalInterceptorController.class);
	}

}
