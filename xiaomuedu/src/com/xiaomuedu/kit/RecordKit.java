package com.xiaomuedu.kit;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.plugin.activerecord.Record;
 
public class RecordKit {
	public static final Record getRecord(String recordName, HttpServletRequest request) {
		return getRecord(recordName,request,false);
	}
    public static final Record getRecord(String recordName, HttpServletRequest request, boolean skipConvertError) {
    	Record record = new Record();
    	Map<String, String[]> paramMap=request.getParameterMap();
    	String start =recordName+".";
    	Object value=null;
    	String[] paramValueArray=null;
    	String paramValue=null;
    	String attrName=null;
    	//循环遍历paramMap
    	for (Entry<String, String[]> param: paramMap.entrySet()) {
    		if(!param.getKey().startsWith(start)){
    			continue;
    		}
    		attrName=param.getKey().replace(start, "");
    		try{
    			paramValueArray=param.getValue();
    			paramValue = (paramValueArray != null && paramValueArray.length > 0) ? paramValueArray[0] : null;
    			value = (paramValue != null&&"".equals(paramValue.trim())==false) ? paramValue : null;
    			//设置值
    			record.set(attrName, value);
    		} catch (Exception e) {
    			if (skipConvertError == false) {
    				throw new RuntimeException(e);
    			}
    		}
    	}
		return record;
    }

   
}