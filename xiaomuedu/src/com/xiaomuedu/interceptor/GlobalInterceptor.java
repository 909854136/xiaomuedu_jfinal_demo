package com.xiaomuedu.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class GlobalInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("全局拦截器 前拦截");
		inv.invoke();
		System.out.println("全局拦截器 后拦截");

	}

}
