<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例020-JSON专题</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">课程案例</div>
  <div class="panel-body" id="jsontopic">
	<li class="list-group-item"><a href="render" >1、renderJson</a></li>
	<li class="list-group-item"><a href="render" >2、JsonKit使用</a></li>
	<li class="list-group-item"><a href="jsontopic/test1" class="ajax" >3、整合第三方-fastJson-test1</a></li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
<script type="text/javascript">
$(function(){
	$("#jsontopic li a.ajax").on("click",function(){
		var _this=$(this);
		var url=_this.attr("href");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				JsonConsoleProcess(data);
			}
		});
		return false;
	});
});
</script>