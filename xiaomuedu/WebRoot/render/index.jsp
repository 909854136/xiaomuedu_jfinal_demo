<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例011-Render系列</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">1、render的用法</div>
  <div class="panel-body">
	 <li class="list-group-item"><a href="render/result1">render("result.jsp")</a></li>
	 <li class="list-group-item"><a href="render/result">render("/render/test/result.jsp")</a></li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">2、renderJsp</div>
  <div class="panel-body">
	 <li class="list-group-item"><a href="render/jsp">renderJsp("result.jsp")</a></li>
  
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">3、renderFreeMarker</div>
  <div class="panel-body">
	 <li class="list-group-item"><a href="render/fm">renderFreeMarker("result.ftl")</a></li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">4、重定向与转发</div>
  <div class="panel-body">
	 <li class="list-group-item"><a href="render/redirect">redirect("/render/redirectResult")</a></li>
	 <li class="list-group-item"><a href="render/redirectwq?param=xiaomuedu">redirect("/render/redirectResult",true)</a></li>
	 <li class="list-group-item"><a href="render/redirecturl">redirect(url)</a></li>
	 <li class="list-group-item"><a href="render/redirect301">redirect301(url)</a></li>
	 <li class="list-group-item"><a href="render/forward">forwardAction("/render/forwardResult")</a></li>
	 <li class="list-group-item"><a href="render/forwardwp">forwardAction("/render/forwardResultwp")</a></li>
  
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">5、renderText</div>
  <div class="panel-body">
	 <li class="list-group-item"><a target="_blank" href="render/text">renderText(text)</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/textxml">renderText(text,xml)</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/texthtml">renderText(text,html)</a></li>
  
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">6、renderHtml</div>
  <div class="panel-body">
	 <li class="list-group-item"><a target="_blank" href="render/html">renderHtml(html)</a></li>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">7、renderXml</div>
  <div class="panel-body">
	 <li class="list-group-item"><a target="_blank" href="render/xml">renderXml(view) 静态</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/xml2">renderXml(view) 动态</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/xml3">renderXml(view) 动态列表</a></li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">8、renderJavascript</div>
  <div class="panel-body">
	 <li class="list-group-item"><a target="_blank" href="render/testjs">进入测试</a></li>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">9、renderError</div>
  <div class="panel-body">
	 <li class="list-group-item"><a target="_blank" href="render/error1">renderError(404)</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/error2">renderError(404,view)指定view</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/error3">renderError(404,render)指定render</a></li>
	 <li class="list-group-item"><a target="_blank" href="render/error4">renderError(404,render)指定render json</a></li>
  
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">10、renderNull</div>
  <div class="panel-body">
  
	 <li class="list-group-item"><a target="_blank" href="render/none">renderNull()</a></li>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">11、renderJson</div>
  <div class="panel-body" id="renderJsonTest">
	 <li class="list-group-item"><a  href="render/json">renderJson()</a></li>
	 <li class="list-group-item"><a  href="render/jsonobj">renderJson(obj)</a></li>
	 <li class="list-group-item"><a  href="render/jsonkv">renderJson(key,value)</a></li>
	 <li class="list-group-item"><a  href="render/jsonmap">renderJson(map)</a></li>
	 <li class="list-group-item"><a  href="render/jsontext">renderJson(jsontext) 使用JsonKit</a></li>
	 <li class="list-group-item"><a  href="render/jsontext2">renderJson(jsontext) 自定义</a></li>
	 <li class="list-group-item"><a  href="render/jsonattrs">renderJson(attrs) </a></li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">JSON控制台</div>
  <div class="panel-body">
   <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<script type="text/javascript">
$(function(){
	$("#renderJsonTest li a").on("click",function(){
		var _this=$(this);
		var url=_this.attr("href");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				JsonConsoleProcess(data);
			}
		});
		return false;
	});
});
</script>
<jsp:include page="../common/footer.jsp"></jsp:include>
