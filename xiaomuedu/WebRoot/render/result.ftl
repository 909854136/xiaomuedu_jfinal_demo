<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>小木学堂</title>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="/static/css/style.css">
<link rel="stylesheet" href="/static/json/s.css">
<link rel="stylesheet" href="/static/css/uploadify.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="/static/js/main.js"></script>
<script src="/static/json/c.js"></script>
<script src="/static/js/jquery.form.js"></script>
<script src="/static/js/jquery.uploadify.min.js"></script>
<script src="/static/js/ajaxupload.js"></script>
</head>
<body>
</head>
<body>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例-Render系列测试结果 freemarker</a>
    </div>
  </div>
</header>
<div id="main" class="container">
<div class="alert alert-success"  style="font-size:18px;">
${msg }
</div>
</div>
<footer>
   <div class="container">
   <div class="row">
   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <span class="bigfont">小木学堂<br>课程案例</span>
   </div>
   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <img data-mask="#qqqrcodeMask" class="qrcode" src="http://xiaomuxuetang.judier.com/upload/4f16b61b-949b-4bfa-b7e1-1e35915e0cc4.png"/>
   <p>点击放大扫描总群</p>
   </div>
   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <img data-mask="#wxqrcodeMask" class="qrcode"  src="http://xiaomuxuetang.judier.com/upload/5327d126-be4b-4e6f-a32e-5a4c4ce0c21b.getqrcode%20(2)"/>
    <p>点击放大扫描关注微信</p>
   </div>
   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
   <div class="alipayqr"><img data-mask="#jfinalqrcodeMask" class="qrcode"  src="http://xiaomuxuetang.judier.com/upload/b734279c-adb2-479d-889b-9dae56fc5225.png"/></div>
    <p>点击放大扫描JFinal分群</p>
   </div>
   </div>
   </div>
   <div class="copyright"><span >© 2015 Powered by  <a href="http://xiaomuxuetang.judier.com">小木学堂</a></span></div>
   </footer>
   <div class="mask" id="qqqrcodeMask" onclick="closeMask()" style="display: none;">
    <div class="qr">
    <img src="http://xiaomuxuetang.judier.com/upload/4f16b61b-949b-4bfa-b7e1-1e35915e0cc4.png">
    </div>
    </div>
   <div class="mask" id="wxqrcodeMask" onclick="closeMask()" style="display: none;">
    <div class="qr">
    <img src="http://xiaomuxuetang.judier.com/upload/5327d126-be4b-4e6f-a32e-5a4c4ce0c21b.getqrcode%20(2)">
    </div>
    </div>
   <div class="mask" id="jfinalqrcodeMask" onclick="closeMask()" style="display: none;">
    <div class="qr">
    <img src="http://xiaomuxuetang.judier.com/upload/b734279c-adb2-479d-889b-9dae56fc5225.png">
    </div>
    </div>
      <script type="text/javascript">
  function closeMask(){
	  $(".mask").fadeOut();
  }
  $(function(){
	  $("footer img").click(function(){
		  $($(this).attr("data-mask")).fadeIn();
	  });
  })
  </script>
    </body>
    </html>