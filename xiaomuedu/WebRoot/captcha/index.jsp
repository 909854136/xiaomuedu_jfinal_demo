<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例018-验证码专题</a>
    </div>
  </div>
</header>
<div id="main" class="container" style="font-size:18px;">
  <div class="row">
  <div class="col-lg-12 col-sm-12 col-md-12">
  <div class="well" style="background:#FFF;margin: 50px auto;width: 500px;">
  <form action="" method="psot">
  <div class="form-group">
  	<input type="text" name="username" class="form-control" placeholder="用户名"/>
  </div>
   <div class="form-group">
  	<input type="password" name="password" class="form-control" placeholder="密码"/>
  </div>
   <div class="form-group">
   <div class="row">
  	<div class="col-sm-6"><input type="text" id="captcha" name="captcha" class="form-control" placeholder="验证码"/></div>
  	<div  class="col-sm-3"><img id="captchaImg" src="captcha/img" width="100" height="35" onclick="changeCaptcha()"/></div>
  	<div  class="col-sm-3"><a href="javascript:changeCaptcha()">换一个</a></div>
   </div>
  </div>
  
  <button class="btn btn-primary" type="button" onclick="validate()">提交</button>
  </form>
  </div>
  
  </div>
  </div>  
</div>
<script>
function changeCaptcha(){
	var img =document.getElementById("captchaImg");
	img.src="captcha/img?t="+new Date().getTime();
}
function validate(){
	var  captcha=$("#captcha").val();
	$.ajax({
		url:"captcha/validate",
		type:"post",
		data:{"captcha":captcha},
		dataType:"json",
		success:function(data){
			changeCaptcha();
			alert(data.msg);
		}
	})
}
</script>
<jsp:include page="../common/footer.jsp"></jsp:include>
