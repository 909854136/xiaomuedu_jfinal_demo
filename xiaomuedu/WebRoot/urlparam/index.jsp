<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例006-URL传参与接参</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">1、key-value:单个参数</div>
  <div class="panel-body">
  <ul class="list-group">
  <li class="list-group-item"><a href="urlparam/keyvalue?p1=1" target="_blank">http://localhost/urlparam/keyvalue?p1=1</a></li>
  <li class="list-group-item"><a href="urlparam/keyvalue?p2=2" target="_blank">http://localhost/urlparam/keyvalue?p2=2</a></li>
  <li class="list-group-item"><a href="urlparam/keyvalue?p3=3" target="_blank">http://localhost/urlparam/keyvalue?p3=3</a></li>
    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">2、key-value:多个参数</div>
  <div class="panel-body">
    <ul class="list-group">
  	<li class="list-group-item"><a href="urlparam/keyvalue?p1=1&p2=2" target="_blank">http://localhost/urlparam/keyvalue?p1=1&p2=2</a></li>
  	<li class="list-group-item"><a href="urlparam/keyvalue?p1=1&p3=3" target="_blank">http://localhost/urlparam/keyvalue?p1=1&p3=3</a></li>
  	<li class="list-group-item"><a href="urlparam/keyvalue?p1=1&p2=2&p3=3" target="_blank">http://localhost/urlparam/keyvalue?p1=1&p2=2&p3=3</a></li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">3、无key类型：单个参数</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/urlparam/nokey/1" target="_blank">http://localhost/urlparam/nokey/1</a></li>
	    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">4、无key类型：多个参数</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/urlparam/nokey/1-2" target="_blank">http://localhost/urlparam/nokey/1-2</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/nokey/1-2-3" target="_blank">http://localhost/urlparam/nokey/1-2-3</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/nokey/1~2~3" target="_blank">http://localhost/urlparam/nokey/1~2~3</a></li>
	    </ul>
    
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">5、参数转型getParaToInt(index)：Integer</div>
  <div class="panel-body">
      <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/urlparam/changetypetoint/1" target="_blank">http://localhost/urlparam/changetypetoint/1-2</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/changetypetoint/1-2" target="_blank">http://localhost/urlparam/changetypetoint/1-2</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/changetypetoint/1-2-3" target="_blank">http://localhost/urlparam/changetypetoint/1-2-3</a></li>
	    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">6、参数转型getParaToInt(key)：Integer</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/urlparam/changetypetoint2?p1=1" target="_blank">http://localhost/urlparam/changetypetoint2?p1=1</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/changetypetoint2?p2=2" target="_blank">http://localhost/urlparam/changetypetoint2?p2=2</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/changetypetoint2?p1=1&p2=2&p3=3" target="_blank">http://localhost/urlparam/changetypetoint2?p1=1&p2=2&p3=3</a></li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">7、参数默认值:getPara(key,defaultValue)</div>
  <div class="panel-body">
      <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/urlparam/defaultvalue?p1=" target="_blank">http://localhost/urlparam/defaultvalue?p1=</a></li>
	    <li class="list-group-item"><a href="http://localhost/urlparam/defaultvalue?p1=&p2=2" target="_blank">http://localhost/urlparam/defaultvalue?p1=&p2=2</a></li>
	    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">8、特殊 传model：getModel</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/urlparam/model?user.name=zhangsan&user.age=10&user.sex=<%=User.SEX_FEMALE %>" target="_blank">http://localhost/urlparam/model?user.name=zhangsan&user.age=10&user.sex=<%=User.SEX_FEMALE %></a></li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
     
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
