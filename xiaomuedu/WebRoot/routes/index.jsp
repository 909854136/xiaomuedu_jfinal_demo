<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例-路由配置 configRoute</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
<div class="alert alert-success" >
配置方式
<pre style="font-size:18px;">
public void configRoute(Routes me) {
	me.add("/", IndexController.class);
	me.add("/routes", RoutesController.class);
}
</pre>
将"/"映射到IndexController 通过配置实现访问http://localhost 将访问IndexController.index()方法。<br>
将"/routes"映射到RoutesController<br>
通过配置实现访问http://localhost/routes将访问RoutesController.index()方法<br>
通过配置实现访问http://localhost/routes/methodName将访问RoutesController.methodName()方法
</div>
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">1、根路径index方法:controllerKey</div>
  <div class="panel-body">
  <ul class="list-group">
  <li class="list-group-item"><a href="http://localhost" target="_blank">http://localhost</a></li>
    <li class="list-group-item"><a href="http://localhost/user" target="_blank">http://localhost/user</a></li>
    <li class="list-group-item"><a href="http://localhost/routes" target="_blank">http://localhost/routes</a></li>
    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">2、访问controller的具体方法:controllerKey/methodName</div>
  <div class="panel-body">
    <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/routes/index" target="_blank">http://localhost/routes/index（无效）</a></li>
	    <li class="list-group-item"><a href="http://localhost/routes/method1" target="_blank">http://localhost/routes/method1</a></li>
	    <li class="list-group-item"><a href="http://localhost/routes/method2" target="_blank">http://localhost/routes/method2</a></li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">3、根路径访问URL挂参：controllerKey/p1-p2</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/routes/1" target="_blank">一个参数：http://localhost/routes/1</a></li>
	    <li class="list-group-item"><a href="http://localhost/routes/1-2" target="_blank">多个参数：http://localhost/routes/1-2</a></li>
	    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">4、访问方法并且传参：controllerKey/method/p1-p2</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/routes/methodParam/1" target="_blank">一个参数：http://localhost/routes/methodParam/1</a></li>
	    <li class="list-group-item"><a href="http://localhost/routes/methodParam/1-2" target="_blank">多个参数：http://localhost/routes/methodParam/1-2</a></li>
	    </ul>
    
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">5、自定义ActionKey:@actionKey("**")</div>
  <div class="panel-body">
      <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/routes/ak" target="_blank">http://localhost/routes/ak</a></li>
	    <li class="list-group-item"><a href="http://localhost/ak" target="_blank">http://localhost/ak</a></li>
	    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">6、自定义ActionKey带参数访问:@actionKey("**")</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/ak/1" target="_blank">http://localhost/ak/1</a></li>
	    <li class="list-group-item"><a href="http://localhost/ak/1-2" target="_blank">http://localhost/ak/1-2</a></li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">7、路由拆分配置</div>
  <div class="panel-body">
      继承Routes自己实现路由拆分，然后在configRoute中配置即可
   <pre style="font-size:18px;">
public void configRoute(Routes me) {
	me.add(new FrontRoutes());
	me.add(new AdminRoutes());
}   
   </pre>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">8、使用Handler扩展路由规则-伪静态</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/routes/method1.html" target="_blank">http://localhost/routes/method1.html</a></li>
	    <li class="list-group-item"><a href="http://localhost/routes/methodParam/1.html" target="_blank">http://localhost/routes/methodParam/1.html</a></li>
	    <li class="list-group-item"><a href="http://localhost/routes/methodParam/1-2.html" target="_blank">http://localhost/routes/methodParam/1-2.html</a></li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
     
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
