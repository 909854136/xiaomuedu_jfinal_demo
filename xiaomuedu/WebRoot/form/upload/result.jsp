<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../../common/header.jsp"></jsp:include>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例-文件上传测试结果</a>
    </div>
  </div>
</header>
<div id="main" class="container">
<div class="alert alert-success"  style="font-size:18px;">
${msg}<br>
上传后的文件磁盘地址：${filePath }
上传后的文件URL：<a href="${url }">${url }</a>
</div>
</div>
<jsp:include page="../../common/footer.jsp"></jsp:include>
