<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<jsp:include page="../../common/header.jsp"></jsp:include>

<header class="navbar navbar-static-top" id="top" role="banner">
	<div class="container">
		<div class="navbar-header" style="text-align: center;">
			<a href="/" class="navbar-brand">小木学堂JFinal课程案例009-010-文件上传-普通Form/AJAX</a>
		</div>
	</div>
</header>
<div id="main" class="container" style="font-size: 18px;">
	<div class="row">
		<div class="col-lg-6 col-sm-6 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">1、普通form上传文件</div>
				<div class="panel-body">
					<form action="upload/formNormal" method="post"
						enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" class="form-control" name="file" />
						</div>
						<button type="submit" class="btn btn-primary">上传文件</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">2、普通form上传文件-带参数</div>
				<div class="panel-body">
					<form action="upload/formwithparam" method="post"
						enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" class="form-control" name="file" />
						</div>

						<div class="form-group">
							<label>参数值：</label> <input type="input" class="form-control"
								name="param" />
						</div>
						<button type="submit" class="btn btn-primary">上传文件</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">3、form表单上传文件 隐藏按钮</div>
				<div class="panel-body">
					<form action="upload/formNormal" method="post"
						enctype="multipart/form-data">
						<label class="btn btn-default"> <span>选择一个文件</span> <input
							type="file" onchange="showFileName(this)" style="display: none;"
							class="form-control" name="file" />
						</label>
						<button type="submit" class="btn btn-primary">上传文件</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-sm-6 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">4、ajax上传文件</div>
				<div class="panel-body">
				<form  method="post"
						enctype="multipart/form-data" id="ajaxForm4">
						<div class="form-group">
							<input type="file" class="form-control" name="file" />
						</div>
						<button type="button" onclick="ajaxUpload('ajaxForm4')" class="btn btn-primary">上传文件</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">5、ajax上传文件-带参数</div>
				<div class="panel-body">
				<form  method="post"
						enctype="multipart/form-data"  id="ajaxForm5">
						<div class="form-group">
							<input type="file" class="form-control" name="file" />
						</div>

						<div class="form-group">
							<label>参数值：</label> <input type="input" class="form-control"
								name="param" />
						</div>
						<button type="button" onclick="ajaxUpload('ajaxForm5')" class="btn btn-primary">上传文件</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-sm-6 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">6、uplodify组件整合</div>
				<div class="panel-body">
				<form  id="uplodifyForm">
							<input type="file" class="form-control" id="uplodifyFile"  />
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">控制台</div>
				<div class="panel-body">
				<div id="Canvas" class="Canvas"></div>
				</div>
			</div>
		</div>
	</div>

</div>
<jsp:include page="../../common/footer.jsp"></jsp:include>
