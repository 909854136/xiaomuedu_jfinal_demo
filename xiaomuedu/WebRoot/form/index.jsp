<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例007-Form表单传参与接参</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">1、普通form提交 post</div>
  <div class="panel-body">
  <form action="form/normal" method="post">
  <div class="form-group">
  <input name="p1" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p2" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p3" class="form-control"  type="text" />
  </div>
  <button type="submit" class="btn btn-primary">提交表单</button>
  </form>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">2、普通form提交 get</div>
  <div class="panel-body">
    <form action="form/get" method="get">
  <div class="form-group">
  <input name="p1" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p2" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p3" class="form-control"  type="text" />
  </div>
  <button type="submit" class="btn btn-primary">提交表单</button>
  </form>
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">3、表单-用户信息新增</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="user/form" target="_blank">http://localhost/user/form</a></li>
	    </ul>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">4、表单-关键词搜索</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item"><a href="http://localhost/user" target="_blank">http://localhost/user</a></li>
	    </ul>
    
  </div>
</div>
  </div>
  </div>  
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">5、表单的target _blank _self</div>
  <div class="panel-body">
        <form action="form/normal" method="post" target="_blank">
  <div class="form-group">
  <input name="p1" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p2" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p3" class="form-control"  type="text" />
  </div>
  <button type="submit" class="btn btn-primary">提交表单</button>
  </form>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">6、表单的target iframe</div>
  <div class="panel-body">
       <form action="form/normal" method="post" target="iframe">
  <div class="form-group">
  <input name="p1" class="form-control" type="text" />
  </div>
  <button type="submit" class="btn btn-primary">提交表单</button>
  </form>
  <iframe name="iframe" width="100%;" height="300px"></iframe>
  </div>
</div>
  </div>
  </div>
  <div class="row">
   <div class="col-lg-12 col-sm-12 col-md-12">
	<div class="panel panel-default">
  <div class="panel-heading">7、表单-其他用途(后面课程讲解)</div>
  <div class="panel-body">
     <ul class="list-group">
	    <li class="list-group-item">文件上传</li>
	    <li class="list-group-item">批量提交</li>
	    <li class="list-group-item">异步提交Form</li>
	    <li class="list-group-item">等等...</li>
	    </ul>
  </div>
</div>
  </div>
  </div>  
     
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
