/**
 * 案例js工具类
 */

//创建一个loading append到body
function newLoading(){
	var loadingObj= $("#appLoading");
	if(loadingObj&&loadingObj.size()==0){
		var loading='<div class="appLoading" style="display: none;" tabindex="-1" id="appLoading"><div class="loading-dialog">'
			+'<div class="loading-msg" id="appLoadingMsg">正在处理...</div><div class="loading-icon" id="appicon"></div></div></div>';
		$("body").append(loading);
	}
}
//创建一个formwaitting append到body
function newFormwaitting(form){
	 var formwaitting= $("#formwaitting");
	 if(formwaitting&&formwaitting.length==0){
		 var loading='<div class="appwaitting" style="display: none;" tabindex="-1" id="formwaitting"><div class="loading-dialog"><div class="loading-msg">正在提交，请稍后...</div><div class="loading-icon"></div></div></div>';
		 $("#"+form).before(loading);
	 }
}
//封装弹出loading
 function appLoading(msg,time,callback){
	 newLoading();
	 if(msg&&msg.length>0){
	    	$("#appLoadingMsg").text(msg);
	    }
      $("#appicon").show();
      $("#appLoading").show();
      if(time&&time>0){
		 setTimeout(function(){
			 $("#appLoading").hide();
			 if(callback){
				 callback();
			 }
		 },time);
      }
  }
 //封装弹出loading
 function appMsg(msg,time){
	 newLoading();
    if(msg&&msg.length>0){
    	$("#appLoadingMsg").text(msg);
    }
      $("#appicon").hide();
      $("#appLoading").show();
      if(time&&time>0){
		 setTimeout(function(){
			 $("#appLoading").hide();
		 },time);
      }
  }
 //封装弹出loading
 function appErrorMsg(msg,time){
	 newLoading();
	 if(msg&&msg.length>0){
		 $("#appLoadingMsg").html("<span class='t-red font-20'><i class='glyphicon glyphicon-warning-sign pr10'></i>"+msg+"</span>");
	 }
	 $("#appicon").hide();
	 $("#appLoading").show();
	 if(time&&time>0){
		 setTimeout(function(){
			 $("#appLoading").hide();
		 },time);
	 }
 }
 //封装弹出loading
 function appSuccessMsg(msg,time){
	 newLoading();
    if(msg&&msg.length>0){
    	$("#appLoadingMsg").html("<span class='t-green font-20'><i class='glyphicon glyphicon-ok pr10'></i>"+msg+"</span>");
    }
      $("#appicon").hide();
      $("#appLoading").show();
      if(time&&time>0){
		 setTimeout(function(){
			 $("#appLoading").hide();
		 },time);
      }
  }
 function closeAppLoading(){
	 setTimeout(function(){
		 $("#appLoading").hide();
	 },200);
 }
  function closeAppLoadingNow(){
      $("#appLoading").hide();
  }
  
  
  
  function showFileName(file){
	  var url=file.value.split("\\");
	  $(file).parent().find("span").text("文件:"+url[url.length-1]);
  }
  
  
  
  /**
   * autoload select zujian
   */
var AutoSelectUtil={
		fillIt:function(select,datas){
			if(datas&&datas.length>0){
				for(var i in datas){
					select.append('<option value="'+datas[i].optionValue+'">'+datas[i].optionText+'</option>');
				}
			}
		},
		load:function(select){
			var url=select.data("url");
			$.ajax({
				type:"get",
				dateType:"json",
				url:url,
				success:function(data){
					if(data.success){
						AutoSelectUtil.fillIt(select,data.result);
					}
				}
			})
		},
		autoload:function(){
			$(".autoloadSelect").each(function(){
				AutoSelectUtil.load($(this)); 
			});
		}
}


$(function(){
	AutoSelectUtil.autoload();
});






