/**
 * ajax传参接参 js
 */

function ajaxget(){
	appLoading("玩儿命加载中...");
	$.ajax({
		type:"get",
		url:"ajax/get",
		dataType:"json",
		data:{"param":"小木学堂"},
		success:function(data){
			JsonConsoleProcess(data);
		}
	})
}
function ajaxpost(){
	appLoading("玩儿命加载中...");
	$.ajax({
		type:"post",
		url:"ajax/post",
		dataType:"json",
		data:{"param":"小木学堂"},
		success:function(data){
			JsonConsoleProcess(data);
		}
	})
}
function ajaxform(){
	appLoading("玩儿命加载中...");
	$.ajax({
		type:"post",
		url:"ajax/form",
		dataType:"json",
		data:$("#ajaxform").serialize(),
		success:function(data){
			JsonConsoleProcess(data);
		}
	})
}