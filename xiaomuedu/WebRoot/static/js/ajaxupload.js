/**
 * ajaxform相关js
 */
function ajaxUpload(formId){
	appLoading("正在上传...");
	$("#"+formId).ajaxSubmit({
		type:"post",
		url:"upload/"+formId,
		dataType:"json",
		success:function(data){
			JsonConsoleProcess(data);
		}
	});
}

$(function() {
	$("#uplodifyFile").uploadify({
		height        : 30,
		swf           : 'static/other/uploadify.swf',
		uploader      : 'upload/uplodify',
		width         : 120,
		onUploadSuccess : function(file, data, response) {
			JsonConsoleProcess(eval("("+data+")"));
        }
	});
});