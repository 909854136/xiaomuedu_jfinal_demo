<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
 <style>
  .row{margin-bottom: 10px;}
  </style>
<script src="static/js/ajax.js"></script>
<script src="static/js/jquery.pin.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例0012-Model系列</a>
    </div>
  </div>
 
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">一、Model课程案例-Model的保存</div>
  <div class="panel-body">
	<li class="list-group-item"><a class="ajax" href="model/setsave">1、set-save</a></li>
	<li class="list-group-item"><a>2、getModel-save</a>
	<form action="model/getsave" id="getsaveForm" method="post" onsubmit="return false;">
	<div class="form-group">
	<div class="col-sm-8">
	<input type="text" class="form-control" name="log.content">
	</div>
	<div class="col-sm-4">
	<button class="btn btn-primary btn-sm" onclick="getsaveFormSubmit()">保存</button>
	</div>
	</div>
	<div class="clearfix"></div>
	</form>
	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">二、Model课程案例-getModel与getBean</div>
  <div class="panel-body">
	<li class="list-group-item">
	<form action="model/getmodel" id="getmodelorbeanForm" method="post" onsubmit="return false;">
	<div class="form-group">
   	<label>姓名</label>
   	<input name="user.name" value="小木"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="form-group">
   	<label>年龄</label>
   	<input  name="user.age" value="28"   type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	<div class="form-group">
   	<label>性别</label>
   	<div class="radio-inline">
   	<input type="radio"  name="user.sex"   checked="checked" value="1"> 男
   	</div>
   	<div class="radio-inline">
   	<input type="radio"   name="user.sex"   value="2"> 女
   	</div>
   	</div>
   	<div class="form-group">
   	<label>备注</label>
   	<textarea  name="user.remark"   style="height:70px;" class="form-control" placeholder="请输入备注信息"　>小木学堂讲师</textarea>
   	</div>
	<div class="clearfix"></div>
	</form>
	</li>
	<li class="list-group-item" style="text-align: center;">
	<button class="btn btn-primary btn-sm" onclick="getmodelFormSubmit()">执行getModel</button>
	<button class="btn btn-primary btn-sm" onclick="getbeanFormSubmit()">执行getBean</button>
	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">三、Model课程案例-Model的查询</div>
  <div class="panel-body">
	<li class="list-group-item"><a target="_blank" href="user">1、普通列表查询搜索</a></li>
	<li class="list-group-item"><a class="ajax" href="model/find">1、查询所有符合条件的find(sql)</a></li>
	<li class="list-group-item"><a  href="javascript:findbyid()">2、根据ID查询 findById(id)</a></li>
	<li class="list-group-item"><a  href="javascript:findByIdLoadColumns()">3、根据ID查询指定列findByIdLoadColumns(id,clomuns)</a></li>
	<li class="list-group-item"><a class="ajax" href="model/findfirst">4、根据条件查询第一个findFirst(sql)</a></li>
	<li class="list-group-item"><a class="ajax" href="model/findfirst2">5、根据条件查询第一个findFirst(sql,params)</a></li>
	<li class="list-group-item"><a  href="javascript:void(0)">6、从缓存查询findByCache</a></li>
	<li class="list-group-item"><a  href="javascript:void(0)">7、从缓存查询findFirstByCache</a></li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">四、Model课程案例-update</div>
  <div class="panel-body">
	<li class="list-group-item">
	<li class="list-group-item"><a  href="javascript:setupdate(1)">1、根据ID set-update</a></li>
	<li class="list-group-item"><a  href="javascript:setupdate(2)">2、根据ID get-set-update</a></li>
	<li class="list-group-item"><a  href="javascript:void(0)">3、getModel-update</a></li>
	<form action="model/getupdate" id="getupdateForm" method="post" onsubmit="return false;">
	<input type="hidden" name="user.id" id="updateFormuserId" value="">
	<div class="form-group">
   	<label>姓名</label>
   	<input name="user.name" value="小木"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="form-group">
   	<label>年龄</label>
   	<input  name="user.age" value="28"   type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	<div class="form-group">
   	<label>性别</label>
   	<div class="radio-inline">
   	<input type="radio"  name="user.sex"   checked="checked" value="1"> 男
   	</div>
   	<div class="radio-inline">
   	<input type="radio"   name="user.sex"   value="2"> 女
   	</div>
   	</div>
   	<div class="form-group">
   	<label>备注</label>
   	<textarea  name="user.remark"   style="height:70px;" class="form-control" placeholder="请输入备注信息"　>小木学堂讲师</textarea>
   	</div>
	<div class="clearfix"></div>
	</form>
	</li>
	<li class="list-group-item" style="text-align: center;">
	<button class="btn btn-primary btn-sm" onclick="getmodelUpdateSubmit()">执行getModel-update</button>
	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">五、Model课程案例-Model的删除</div>
  <div class="panel-body">
	<li class="list-group-item"><a href="javascript:testdel('model/setdelete/')">1、set-delete</a></li>
	<li class="list-group-item"><a href="javascript:testdel('model/getdelete/')">2、get-delete</a></li>
	<li class="list-group-item"><a href="javascript:testdel('model/delbyid/')">3、deleteById</a></li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">六、Model课程案例-Model的批量接收与保存</div>
  <div class="panel-body">
  <form action="model/getmodelssave" id="getmodelssaveForm" method="post" onsubmit="return false;">
	<div class="form-group" style="text-align: center;">
   	<label class="control-label col-sm-6" >姓名</label>
   	<label class="control-label col-sm-6">年龄</label>
   	</div>
   	<div class="row">
	<div class="form-group">
   	<div class="col-sm-6">
   	<input name="user.name" value="小木"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="col-sm-6">
   	<input name="user.age" value="28"  type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	</div>
   	</div>
   	<div class="row">
	<div class="form-group">
   	<div class="col-sm-6">
   	<input name="user.name" value="小木2016"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="col-sm-6">
   	<input name="user.age" value="27"  type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	</div>
   	</div>
   		<li class="list-group-item" style="text-align: center;">
	<button type="button" class="btn btn-primary btn-sm" onclick="getmodelssaveSubmit()">执行批量提交/保存</button>
	</li>
   	</form>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">七、Model课程案例-Model的批量修改</div>
  <div class="panel-body">
  <li class="list-group-item"  >
	<fieldset>
	<legend>1、批量按照ID更新</legend>
</fieldset>
	<form action="model/getmodelsupdate" id="getmodelsupdateForm"
						method="post" onsubmit="return false;">
		<div class="form-group" style="text-align: center;">
			<label class="control-label col-sm-4">ID</label>
			<label class="control-label col-sm-4">姓名</label> 
			<label class="control-label col-sm-4">年龄</label>
		</div>
		<c:forEach items="${users }" var="user" varStatus="index">
			<div class="row">
				<div class="form-group">
					<div class="col-sm-4">
						<input type="hidden" value="${user.id }" name="user[${index.index }].id" />
						<p class="form-control-static" style="text-align: center;">${user.id }</p>
					</div>
					<div class="col-sm-4">
						<input name="user[${index.index }].name" value="${user.name }" type="text"
							class="form-control" placeholder="请输入姓名">
					</div>
					<div class="col-sm-4">
						<input name="user[${index.index }].age" value="${user.age }" type="text"
							class="form-control" placeholder="请输入年龄">
					</div>
				</div>
			</div>
		</c:forEach>
		<li class="list-group-item" style="text-align: center;">
			<button type="button" class="btn btn-primary btn-sm"
				onclick="getmodelsupdateSubmit()">执行批量提交/修改</button>
		</li>
	</form>
</li>
  <li class="list-group-item"  >
	<fieldset>
	<legend>2、批量按照条件更新</legend>
	</fieldset>
   <li class="list-group-item" style="text-align: center;">
	<a href="model/updatemodels1" class="ajax">更新所有男变女</a>
	</li>
   <li class="list-group-item" style="text-align: center;">
	<a href="model/updatemodels4" class="ajax">更新所有女变男</a>
	</li>
    <li class="list-group-item" style="text-align: center;">
	<a href="model/updatemodels2" class="ajax">更新所有女年龄21</a>
	</li>
    <li class="list-group-item" style="text-align: center;">
	<a href="model/updatemodels3" class="ajax">更新所有名字带小木的年龄28</a>
	</li>
   	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">八、Model课程案例-Model的批量删除</div>
  <div class="panel-body">
  <li class="list-group-item"  >
	<fieldset>
	<legend>1、批量按照ID删除</legend>
</fieldset>
	<form action="model/batchdel" id="batchdelForm"
						method="post" onsubmit="return false;">
		<div class="form-group" style="text-align: center;">
			<label class="control-label col-sm-2">选择</label>
			<label class="control-label col-sm-2">ID</label>
			<label class="control-label col-sm-4">姓名</label> 
			<label class="control-label col-sm-2">年龄</label> 
			<label class="control-label col-sm-2">性别</label> 
		</div>
		<c:forEach items="${users }" var="user" varStatus="index">
			<div class="row">
				<div class="form-group">
					<div class="col-sm-2" style="text-align: center;">
						<div class="checkbox-inline">
						<label>
						<input type="checkbox" name="id" value="${user.id }">
						</label>
						</div>
					</div>
					<div class="col-sm-2">
						<p class="form-control-static" style="text-align: center;">${user.id }</p>
					</div>
					<div class="col-sm-4">
						<p class="form-control-static" style="text-align: center;">${user.name }</p>
					</div>
					<div class="col-sm-2">
						<p class="form-control-static" style="text-align: center;">${user.age }</p>
					</div>
					<div class="col-sm-2">
						<p class="form-control-static" style="text-align: center;">${user.sex==1?"男":"女" }</p>
					</div>
				</div>
			</div>
		</c:forEach>
		<li class="list-group-item" style="text-align: center;">
			<button type="button" class="btn btn-primary btn-sm"
				onclick="batchdelFormSubmit()">执行批量提交/删除</button>
		</li>
	</form>
</li>

 <li class="list-group-item"  >
	<fieldset>
	<legend>2、批量按照条件删除</legend>
	</fieldset>
  	<li class="list-group-item" style="text-align: center;">
	<a href="model/batchdel2" class="ajax">删除所有年龄小于18的女生</a>
	</li>
  	<li class="list-group-item" style="text-align: center;">
	<a href="model/batchdel3" class="ajax">删除所有姓名带小木的</a>
	</li>
   	</li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6" >
	<div class="panel panel-default pinned" style="margin-top: 10px;">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<script type="text/javascript">
$(function(){
	$(".pinned").pin();
	$("li a.ajax").on("click",function(){
		var _this=$(this);
		var url=_this.attr("href");
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
		return false;
	});
});

function getmodelFormSubmit(){
	appLoading("执行中...");
	$.post("model/getmodel",$("#getmodelorbeanForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getsaveFormSubmit(){
	appLoading("执行中...");
	$.post("model/getmodel",$("#getmodelorbeanForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getbeanFormSubmit(){
	appLoading("执行中...");
	$.post("model/getbean",$("#getmodelorbeanForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}


function findbyid(){
	var id=prompt("输入一个id", 1);
	if(id){
		var url="model/findbyid/"+id;
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}
function findByIdLoadColumns(){
	var id=prompt("输入一个id", 1);
	var columns=prompt("输入查询的字段", "id,name,age,sex,remark");
	if(id&&columns){
		var url="model/findByIdLoadColumns";
		appLoading("执行中...");
		$.ajax({
			type:"post",
			url:url,
			data:{"id":id,"columns":columns},
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}
function setupdate(index){
	var id=prompt("输入一个id", 1);
	var name=prompt("输入name的值", "小木2016");
	if(id&&name){
		var url="model/setupdate"+index;
		appLoading("执行中...");
		$.ajax({
			type:"post",
			url:url,
			data:{"id":id,"name":name},
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}

function getmodelUpdateSubmit(){
	var id=prompt("输入一个id", 1);
	if(id){
		$("#updateFormuserId").val(id);
		appLoading("执行中...");
		$.post("model/getupdate",$("#getupdateForm").serialize(),function(data){
			closeAppLoading();
			$("#updateFormuserId").val("");
			JsonConsoleProcess(data);
		});
	}
}


function testdel(url){
	var id=prompt("输入一个id", 1);
	if(id){
		url=url+id;
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}

function getmodelssaveSubmit(){
	appLoading("执行中...");
	$.post("model/getmodelssave",$("#getmodelssaveForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getmodelsupdateSubmit(){
	appLoading("执行中...");
	$.post("model/getmodelsupdate",$("#getmodelsupdateForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getmodelsupdateSubmit(){
	appLoading("执行中...");
	$.post("model/getmodelsupdate",$("#getmodelsupdateForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function batchdelFormSubmit(){
	appLoading("执行中...");
	$.post("model/batchdel",$("#batchdelForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
		$("#batchdelForm").find("input[name='id']:checked").closest(".row").remove();
	});
}

</script>
<jsp:include page="../common/footer.jsp"></jsp:include>
