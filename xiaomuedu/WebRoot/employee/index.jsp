<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
 <style>
  .row{margin-bottom: 10px;}
  </style>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例0014-表关联查询(员工查询)</a>
    </div>
  </div>
 
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">表关联查询案例</div>
  <div class="panel-body">
	<li class="list-group-item"><a class="ajax" href="employee/normal">1、普通查询</a></li>
	<li class="list-group-item"><a class="ajax" href="employee/leftjoin">2、left join查询出部门名称</a></li>
	<li class="list-group-item"><a class="ajax" href="employee/rightjoin">3、right join查询出部门名称</a></li>
	<li class="list-group-item"><a class="ajax" href="employee/innerjoin">4、inner join查询出部门名称</a></li>
	<li class="list-group-item"><a class="ajax" href="employee/fulljoin">5、full join查询出部门名称</a></li>
	<li class="list-group-item">----------------</li>
	<li class="list-group-item"><a class="ajax" href="employee/or">6、or查询</a></li>
	<li class="list-group-item"><a class="ajax" href="employee/unionjoin">7、union查询</a></li>
  </div>
</div>
</div>
  <div class="col-lg-6 col-sm-6 col-md-6" >
	<div class="panel panel-default pinned" style="margin-top: 10px;">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<script type="text/javascript">
$(function(){
	$("li a.ajax").on("click",function(){
		var _this=$(this);
		var url=_this.attr("href");
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
		return false;
	});
});

function getrecordFormSubmit(){
	appLoading("执行中...");
	$.post("record/getrecord",$("#getrecordorbeanForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getsaveFormSubmit(){
	appLoading("执行中...");
	$.post("record/getsave",$("#getsaveForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}


function findbyid(){
	var id=prompt("输入一个id", 1);
	if(id){
		var url="record/findbyid/"+id;
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}
function findByIdLoadColumns(){
	var id=prompt("输入一个id", 1);
	var columns=prompt("输入查询的字段", "id,name,age,sex,remark");
	if(id&&columns){
		var url="record/findByIdLoadColumns";
		appLoading("执行中...");
		$.ajax({
			type:"post",
			url:url,
			data:{"id":id,"columns":columns},
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}
function setupdate(index){
	var id=prompt("输入一个id", 1);
	var name=prompt("输入name的值", "小木2016");
	if(id&&name){
		var url="record/setupdate"+index;
		appLoading("执行中...");
		$.ajax({
			type:"post",
			url:url,
			data:{"id":id,"name":name},
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}

function getrecordUpdateSubmit(){
	var id=prompt("输入一个id", 1);
	if(id){
		$("#updateFormuserId").val(id);
		appLoading("执行中...");
		$.post("record/getupdate",$("#getupdateForm").serialize(),function(data){
			closeAppLoading();
			$("#updateFormuserId").val("");
			JsonConsoleProcess(data);
		});
	}
}


function testdel(url){
	var id=prompt("输入一个id", 1);
	if(id){
		url=url+id;
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}

function getrecordssaveSubmit(){
	appLoading("执行中...");
	$.post("record/getrecordssave",$("#getrecordssaveForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getrecordsupdateSubmit(){
	appLoading("执行中...");
	$.post("record/getrecordsupdate",$("#getrecordsupdateForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getrecordsupdateSubmit(){
	appLoading("执行中...");
	$.post("record/getrecordsupdate",$("#getrecordsupdateForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function batchdelFormSubmit(){
	appLoading("执行中...");
	$.post("record/batchdel",$("#batchdelForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
		$("#batchdelForm").find("input[name='id']:checked").closest(".row").remove();
	});
}

</script>
<jsp:include page="../common/footer.jsp"></jsp:include>
