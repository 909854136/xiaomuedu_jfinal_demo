<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
 <style>
  .row{margin-bottom: 10px;}
  </style>
<script src="static/js/ajax.js"></script>
<script src="static/js/jquery.pin.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例0013-Db+Record系列</a>
    </div>
  </div>
 
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">一、Record课程案例-Record的保存</div>
  <div class="panel-body">
	<li class="list-group-item"><a class="ajax" href="record/setsave">1、set-save</a></li>
	<li class="list-group-item"><a>2、getRecord-save</a>
	<form action="record/getsave" id="getsaveForm" method="post" onsubmit="return false;">
	<div class="form-group">
	<div class="col-sm-8">
	<input type="text" class="form-control" name="log.content">
	</div>
	<div class="col-sm-4">
	<button class="btn btn-primary btn-sm" onclick="getsaveFormSubmit()">保存</button>
	</div>
	</div>
	<div class="clearfix"></div>
	</form>
	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">二、Record课程案例-getRecord</div>
  <div class="panel-body">
	<li class="list-group-item">
	<form action="record/getrecord" id="getrecordorbeanForm" method="post" onsubmit="return false;">
	<div class="form-group">
   	<label>姓名</label>
   	<input name="user.name" value="小木"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="form-group">
   	<label>年龄</label>
   	<input  name="user.age" value="28"   type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	<div class="form-group">
   	<label>性别</label>
   	<div class="radio-inline">
   	<input type="radio"  name="user.sex"   checked="checked" value="1"> 男
   	</div>
   	<div class="radio-inline">
   	<input type="radio"   name="user.sex"   value="2"> 女
   	</div>
   	</div>
   	<div class="form-group">
   	<label>备注</label>
   	<textarea  name="user.remark"   style="height:70px;" class="form-control" placeholder="请输入备注信息"　>小木学堂讲师</textarea>
   	</div>
	<div class="clearfix"></div>
	</form>
	</li>
	<li class="list-group-item" style="text-align: center;">
	<button class="btn btn-primary btn-sm" onclick="getrecordFormSubmit()">执行getRecord</button>
	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">三、Record课程案例-Record的查询</div>
  <div class="panel-body">
	<li class="list-group-item"><a class="ajax" href="record/find">1、查询所有符合条件的Db.find(sql)</a></li>
	<li class="list-group-item"><a  href="javascript:findbyid()">2、根据ID查询 Db.findById(id)</a></li>
	<li class="list-group-item"><a  href="javascript:findByIdLoadColumns()">3、根据ID查询指定列Db.findFirst(sql)</a></li>
	<li class="list-group-item"><a class="ajax" href="record/findfirst">4、根据条件查询第一个Db.findFirst(sql)</a></li>
	<li class="list-group-item"><a class="ajax" href="record/findfirst2">5、根据条件查询第一个Db.findFirst(sql,params)</a></li>
	<li class="list-group-item"><a  href="javascript:void(0)">6、从缓存查询Db.findByCache</a></li>
	<li class="list-group-item"><a  href="javascript:void(0)">7、从缓存查询Db.findFirstByCache</a></li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">四、Record课程案例-update</div>
  <div class="panel-body">
	<li class="list-group-item">
	<li class="list-group-item"><a  href="javascript:setupdate(1)">1、根据ID set-update</a></li>
	<li class="list-group-item"><a  href="javascript:setupdate(2)">2、根据ID get-set-update</a></li>
	<li class="list-group-item"><a  href="javascript:void(0)">3、getRecord-update</a></li>
	<form action="record/getupdate" id="getupdateForm" method="post" onsubmit="return false;">
	<input type="hidden" name="user.id" id="updateFormuserId" value="">
	<div class="form-group">
   	<label>姓名</label>
   	<input name="user.name" value="小木"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="form-group">
   	<label>年龄</label>
   	<input  name="user.age" value="28"   type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	<div class="form-group">
   	<label>性别</label>
   	<div class="radio-inline">
   	<input type="radio"  name="user.sex"   checked="checked" value="1"> 男
   	</div>
   	<div class="radio-inline">
   	<input type="radio"   name="user.sex"   value="2"> 女
   	</div>
   	</div>
   	<div class="form-group">
   	<label>备注</label>
   	<textarea  name="user.remark"   style="height:70px;" class="form-control" placeholder="请输入备注信息"　>小木学堂讲师</textarea>
   	</div>
	<div class="clearfix"></div>
	</form>
	</li>
	<li class="list-group-item" style="text-align: center;">
	<button class="btn btn-primary btn-sm" onclick="getrecordUpdateSubmit()">执行getRecord-update</button>
	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">五、Record课程案例-Record的删除</div>
  <div class="panel-body">
	<li class="list-group-item"><a href="javascript:testdel('record/setdelete/')">1、set-delete</a></li>
	<li class="list-group-item"><a href="javascript:testdel('record/getdelete/')">2、get-delete</a></li>
	<li class="list-group-item"><a href="javascript:testdel('record/delbyid/')">3、deleteById</a></li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">六、Record课程案例-Record的批量接收与保存</div>
  <div class="panel-body">
  <form action="record/getrecordssave" id="getrecordssaveForm" method="post" onsubmit="return false;">
	<div class="form-group" style="text-align: center;">
   	<label class="control-label col-sm-6" >姓名</label>
   	<label class="control-label col-sm-6">年龄</label>
   	</div>
   	<div class="row">
	<div class="form-group">
   	<div class="col-sm-6">
   	<input name="user[0].name" value="小木"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="col-sm-6">
   	<input name="user[0].age" value="28"  type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	</div>
   	</div>
   	<div class="row">
	<div class="form-group">
   	<div class="col-sm-6">
   	<input name="user[1].name" value="小木2016"  type="text" class="form-control" placeholder="请输入姓名">
   	</div>
   	<div class="col-sm-6">
   	<input name="user[1].age" value="27"  type="text" class="form-control" placeholder="请输入年龄">
   	</div>
   	</div>
   	</div>
   		<li class="list-group-item" style="text-align: center;">
	<button type="button" class="btn btn-primary btn-sm" onclick="getrecordssaveSubmit()">执行批量提交/保存</button>
	</li>
   	</form>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">七、Record课程案例-Record的批量修改</div>
  <div class="panel-body">
  <li class="list-group-item"  >
	<fieldset>
	<legend>1、批量按照ID更新</legend>
</fieldset>
	<form action="record/getrecordsupdate" id="getrecordsupdateForm"
						method="post" onsubmit="return false;">
		<div class="form-group" style="text-align: center;">
			<label class="control-label col-sm-4">ID</label>
			<label class="control-label col-sm-4">姓名</label> 
			<label class="control-label col-sm-4">年龄</label>
		</div>
		<c:forEach items="${users }" var="user" varStatus="index">
			<div class="row">
				<div class="form-group">
					<div class="col-sm-4">
						<input type="hidden" value="${user.id }" name="user[${index.index }].id" />
						<p class="form-control-static" style="text-align: center;">${user.id }</p>
					</div>
					<div class="col-sm-4">
						<input name="user[${index.index }].name" value="${user.name }" type="text"
							class="form-control" placeholder="请输入姓名">
					</div>
					<div class="col-sm-4">
						<input name="user[${index.index }].age" value="${user.age }" type="text"
							class="form-control" placeholder="请输入年龄">
					</div>
				</div>
			</div>
		</c:forEach>
		<li class="list-group-item" style="text-align: center;">
			<button type="button" class="btn btn-primary btn-sm"
				onclick="getrecordsupdateSubmit()">执行批量提交/修改</button>
		</li>
	</form>
</li>
  <li class="list-group-item"  >
	<fieldset>
	<legend>2、批量按照条件更新</legend>
	</fieldset>
   <li class="list-group-item" style="text-align: center;">
	<a href="record/updaterecords1" class="ajax">更新所有男变女</a>
	</li>
   <li class="list-group-item" style="text-align: center;">
	<a href="record/updaterecords4" class="ajax">更新所有女变男</a>
	</li>
    <li class="list-group-item" style="text-align: center;">
	<a href="record/updaterecords2" class="ajax">更新所有女年龄21</a>
	</li>
    <li class="list-group-item" style="text-align: center;">
	<a href="record/updaterecords3" class="ajax">更新所有名字带小木的年龄28</a>
	</li>
   	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">八、Record课程案例-Record的批量删除</div>
  <div class="panel-body">
  <li class="list-group-item"  >
	<fieldset>
	<legend>1、批量按照ID删除</legend>
</fieldset>
	<form action="record/batchdel" id="batchdelForm"
						method="post" onsubmit="return false;">
		<div class="form-group" style="text-align: center;">
			<label class="control-label col-sm-2">选择</label>
			<label class="control-label col-sm-2">ID</label>
			<label class="control-label col-sm-4">姓名</label> 
			<label class="control-label col-sm-2">年龄</label> 
			<label class="control-label col-sm-2">性别</label> 
		</div>
		<c:forEach items="${users }" var="user" varStatus="index">
			<div class="row">
				<div class="form-group">
					<div class="col-sm-2" style="text-align: center;">
						<div class="checkbox-inline">
						<label>
						<input type="checkbox" name="id" value="${user.id }">
						</label>
						</div>
					</div>
					<div class="col-sm-2">
						<p class="form-control-static" style="text-align: center;">${user.id }</p>
					</div>
					<div class="col-sm-4">
						<p class="form-control-static" style="text-align: center;">${user.name }</p>
					</div>
					<div class="col-sm-2">
						<p class="form-control-static" style="text-align: center;">${user.age }</p>
					</div>
					<div class="col-sm-2">
						<p class="form-control-static" style="text-align: center;">${user.sex==1?"男":"女" }</p>
					</div>
				</div>
			</div>
		</c:forEach>
		<li class="list-group-item" style="text-align: center;">
			<button type="button" class="btn btn-primary btn-sm"
				onclick="batchdelFormSubmit()">执行批量提交/删除</button>
		</li>
	</form>
</li>

 <li class="list-group-item"  >
	<fieldset>
	<legend>2、批量按照条件删除</legend>
	</fieldset>
  	<li class="list-group-item" style="text-align: center;">
	<a href="record/batchdel2" class="ajax">删除所有年龄小于18的女生</a>
	</li>
  	<li class="list-group-item" style="text-align: center;">
	<a href="record/batchdel3" class="ajax">删除所有姓名带小木的</a>
	</li>
   	</li>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">九、Record课程案例-Model与Record互转</div>
  <div class="panel-body">
 <li class="list-group-item" style="text-align: center;">
	<a href="record/modeltorecord" class="ajax">model转record</a>
	</li>
  <li class="list-group-item" style="text-align: center;">
	<a href="record/recordtomodel" class="ajax">record转model</a>
  </li>
  <li class="list-group-item" style="text-align: center;">
	<a  >其它转</a>
  </li>
</div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6" >
	<div class="panel panel-default pinned" style="margin-top: 10px;">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<script type="text/javascript">
$(function(){
	$(".pinned").pin();
	$("li a.ajax").on("click",function(){
		var _this=$(this);
		var url=_this.attr("href");
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
		return false;
	});
});

function getrecordFormSubmit(){
	appLoading("执行中...");
	$.post("record/getrecord",$("#getrecordorbeanForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getsaveFormSubmit(){
	appLoading("执行中...");
	$.post("record/getsave",$("#getsaveForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}


function findbyid(){
	var id=prompt("输入一个id", 1);
	if(id){
		var url="record/findbyid/"+id;
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}
function findByIdLoadColumns(){
	var id=prompt("输入一个id", 1);
	var columns=prompt("输入查询的字段", "id,name,age,sex,remark");
	if(id&&columns){
		var url="record/findByIdLoadColumns";
		appLoading("执行中...");
		$.ajax({
			type:"post",
			url:url,
			data:{"id":id,"columns":columns},
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}
function setupdate(index){
	var id=prompt("输入一个id", 1);
	var name=prompt("输入name的值", "小木2016");
	if(id&&name){
		var url="record/setupdate"+index;
		appLoading("执行中...");
		$.ajax({
			type:"post",
			url:url,
			data:{"id":id,"name":name},
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}

function getrecordUpdateSubmit(){
	var id=prompt("输入一个id", 1);
	if(id){
		$("#updateFormuserId").val(id);
		appLoading("执行中...");
		$.post("record/getupdate",$("#getupdateForm").serialize(),function(data){
			closeAppLoading();
			$("#updateFormuserId").val("");
			JsonConsoleProcess(data);
		});
	}
}


function testdel(url){
	var id=prompt("输入一个id", 1);
	if(id){
		url=url+id;
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
	}
}

function getrecordssaveSubmit(){
	appLoading("执行中...");
	$.post("record/getrecordssave",$("#getrecordssaveForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getrecordsupdateSubmit(){
	appLoading("执行中...");
	$.post("record/getrecordsupdate",$("#getrecordsupdateForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function getrecordsupdateSubmit(){
	appLoading("执行中...");
	$.post("record/getrecordsupdate",$("#getrecordsupdateForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
	});
}
function batchdelFormSubmit(){
	appLoading("执行中...");
	$.post("record/batchdel",$("#batchdelForm").serialize(),function(data){
		closeAppLoading();
		JsonConsoleProcess(data);
		$("#batchdelForm").find("input[name='id']:checked").closest(".row").remove();
	});
}

</script>
<jsp:include page="../common/footer.jsp"></jsp:include>
