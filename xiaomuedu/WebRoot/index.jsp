<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<jsp:include page="common/header.jsp"></jsp:include>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例</a>
    </div>
  </div>
</header>
<div id="main" class="container" style="font-size:16px;">
    <div class="list-group">
	  <a  class="list-group-item" href="user">案例001：用户管理之快速实现CURD</a>
	  <a  class="list-group-item" href="routes">案例002：JFinal配置-路由配置测试</a>
	  <a  class="list-group-item" href="routes">案例003：JFinal配置-插件配置测试 直接看XiaomuPlugin 看控制台输出</a>
	  <a  class="list-group-item" href="globalInterceptor">案例004：JFinal配置-全局拦截器配置测试 看控制台输出</a>
	  <a  class="list-group-item" href="routes">案例005：JFinal配置-处理器配置测试  看控制台输出</a>
	  <a  class="list-group-item" href="urlparam">案例006：JFinal传参-URL传参与后台接参</a>
	  <a  class="list-group-item" href="form">案例007：JFinal传参-Form传参与后台接参</a>
	  <a  class="list-group-item" href="ajax">案例008：JFinal传参-AJAX传参与后台接参</a>
	  <a  class="list-group-item" href="upload">案例009：JFinal传参-文件上传-普通form</a>
	  <a  class="list-group-item" href="upload">案例010：JFinal传参-文件上传-AJAX</a>
	  <a  class="list-group-item" href="render">案例011：JFinal Render系列-视图响应</a>
	  <a  class="list-group-item" href="model">案例012：JFinal Model系列</a>
	  <a  class="list-group-item" href="record">案例013：JFinal Db+Record系列</a>
	  <a  class="list-group-item" href="area">直播分享课：autoload select</a>
	  <a  class="list-group-item" href="employee">案例014：表关联查询(员工查询)</a>
	  <a  class="list-group-item" href="captcha">案例018：验证码专题</a>
	  <a  class="list-group-item" href="pv">案例019：数据校验专题</a>
	  <a  class="list-group-item" href="jsontopic">案例020：JSON专题</a>
	  
	</div>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
     
</div>
<jsp:include page="common/footer.jsp"></jsp:include>
