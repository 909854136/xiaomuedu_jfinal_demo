<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例019-数据校验专题</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">课程案例</div>
  <div class="panel-body">
	<li class="list-group-item">
	<a href="javascript:void(0)">1、前端HTML自带检验</a>
	<form method="post" action="">
	<div class="form-group">
	<input class="form-control" required="required" type="text" maxlength="20" placeholder="字符串，长度最大20"/>
	</div>
	<div class="form-group">
	<input class="form-control" required="required" type="number" min="1" max="6"  placeholder="数字"/>
	</div>
	<div class="form-group">
	<input class="form-control" required="required" type="date"   placeholder="日期"/>
	</div>
	<button class="btn btn-primary">提交校验</button>
	</form>
	</li>
	<li class="list-group-item">
	<a href="javascript:void(0)">2、前端JS+自定义属性检验</a>
	<form method="post" action="" onsubmit="return FormChecker.check(this);">
	<div class="form-group">
	<input class="form-control" type="text" data-tips="此项必填，长度小于等于20" data-rule="required;len<=20" maxlength="20" placeholder="字符串，长度最大20"/>
	</div>
	<button class="btn btn-primary" >提交JS校验</button>
	</form>
	</li>
	<li class="list-group-item">
	<a href="javascript:void(0)">3、后端校验</a>
	<form method="post" >
	<div class="form-group">
	<input id="param1" class="form-control" type="text" data-tips="此项必填，长度小于等于20" data-rule="required;len<=20" maxlength="20" placeholder="字符串，长度最大20"/>
	</div>
	<button class="btn btn-primary" type="button" onclick="submit1()" >提交JS校验</button>
	</form>
	</li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
<script>
var FormChecker={
	check:function(formEle){
		var form=$(formEle);
		var checkResult=false;
		form.find("input[data-rule]").each(function(){
			checkResult=FormChecker.checkInput($(this));
			if(checkResult==false){
				return;
			}
		});
		return checkResult;
	},
	checkInput:function(input){
		//检测一个输入框的数据校验
		var dataRule=input.data("rule");
		var rules=null;
		if(dataRule.indexOf(";")!=-1){
			rules=dataRule.split(";");
		}
		if(rules!=null&&rules.length>0){
			for(var i in rules){
				if(FormChecker.checkInputOneRule(input,rules[i])==false){
					return false;
				}
			}
		}else{
			var value=$.trim(input.val());
			var tips=input.data("tips");
			if(dataRule=="required"&&!value){
				alert(tips);
				return false;
			}
		}
		
		return true;
	},
	checkInputOneRule:function(input,rule){
		var value=$.trim(input.val());
		var tips=input.data("tips");
		var result=true;
		if(rule=="required"&&!value){
			alert(tips);
			result== false;
		}else if(rule.indexOf("len")!=-1){
			var vl=value.length;
			if(rule.indexOf("<=")){
				var cl=Number(rule.substring(5,rule.length));
				if(vl>cl){
					alert(tips);
					result== false;
				}
			}
			
		}
		return result;
	}
	
}

function submit1(){
	var param=$.trim($("#param1").val());
	$.post("pv/submit",{"param":param},function(data){
		JsonConsoleProcess(data);
	});
	}
</script>