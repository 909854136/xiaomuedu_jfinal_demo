<%@page import="com.xiaomuedu.common.model.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例008-AJAX传参与接参</a>
    </div>
  </div>
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">课程案例</div>
  <div class="panel-body">
	<li class="list-group-item"><a href="javascript:ajaxget()">1、AJAX GET</a></li>
	<li class="list-group-item"><a href="javascript:ajaxpost()">2、AJAX POST</a></li>
	<li class="list-group-item">
	<a href="javascript:void(0)">3、AJAX Form</a>
	  <form action="ajax/form" method="post" id="ajaxform">
  <div class="form-group">
  <input name="p1" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p2" class="form-control" type="text" />
  </div>
  <div class="form-group">
  <input name="p3" class="form-control"  type="text" />
  </div>
  <button type="button" onclick="ajaxform()" class="btn btn-primary">提交表单</button>
  </form>
	
	</li>
  </div>
</div>
  </div>
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
