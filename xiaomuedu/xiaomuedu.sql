/*
Navicat MySQL Data Transfer

Source Server         : 小木学堂
Source Server Version : 50703
Source Host           : 127.0.0.1:3306
Source Database       : xiaomuedu

Target Server Type    : MYSQL
Target Server Version : 50703
File Encoding         : 65001

Date: 2016-06-14 15:46:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('2', '系统启动之后回调afterJfinalStart', '2016-04-28 21:52:30');
INSERT INTO `log` VALUES ('3', '系统关闭之前回调beforeJFinalStop', '2016-04-28 21:53:11');
INSERT INTO `log` VALUES ('4', '系统启动之后回调afterJfinalStart', '2016-04-28 21:53:14');
INSERT INTO `log` VALUES ('5', '系统启动之后回调afterJfinalStart', '2016-05-05 21:25:57');
INSERT INTO `log` VALUES ('6', '系统关闭之前回调beforeJFinalStop', '2016-05-05 21:27:21');
INSERT INTO `log` VALUES ('7', '系统启动之后回调afterJfinalStart', '2016-05-05 21:27:22');
INSERT INTO `log` VALUES ('8', '系统启动之后回调afterJfinalStart', '2016-05-05 21:37:27');
INSERT INTO `log` VALUES ('9', '系统关闭之前回调beforeJFinalStop', '2016-05-05 21:41:26');
INSERT INTO `log` VALUES ('10', '系统启动之后回调afterJfinalStart', '2016-05-05 21:41:28');
INSERT INTO `log` VALUES ('11', '系统启动之后回调afterJfinalStart', '2016-05-05 21:42:57');
INSERT INTO `log` VALUES ('12', '系统关闭之前回调beforeJFinalStop', '2016-05-05 21:45:40');
INSERT INTO `log` VALUES ('13', '系统启动之后回调afterJfinalStart', '2016-05-05 21:45:42');
INSERT INTO `log` VALUES ('14', '系统关闭之前回调beforeJFinalStop', '2016-05-05 21:45:50');
INSERT INTO `log` VALUES ('15', '系统启动之后回调afterJfinalStart', '2016-05-05 21:45:53');
INSERT INTO `log` VALUES ('16', '系统关闭之前回调beforeJFinalStop', '2016-05-05 21:46:20');
INSERT INTO `log` VALUES ('17', '系统启动之后回调afterJfinalStart', '2016-05-05 21:46:23');
INSERT INTO `log` VALUES ('18', '系统关闭之前回调beforeJFinalStop', '2016-05-05 21:53:55');
INSERT INTO `log` VALUES ('19', '系统启动之后回调afterJfinalStart', '2016-05-05 21:53:57');
INSERT INTO `log` VALUES ('20', '系统启动之后回调afterJfinalStart', '2016-05-05 21:59:13');
INSERT INTO `log` VALUES ('21', '系统启动之后回调afterJfinalStart', '2016-05-05 22:00:37');
INSERT INTO `log` VALUES ('22', '系统启动之后回调afterJfinalStart', '2016-05-05 22:16:38');
INSERT INTO `log` VALUES ('23', '系统关闭之前回调beforeJFinalStop', '2016-05-05 22:26:52');
INSERT INTO `log` VALUES ('24', '系统启动之后回调afterJfinalStart', '2016-05-05 22:26:53');
INSERT INTO `log` VALUES ('25', '系统启动之后回调afterJfinalStart', '2016-05-05 22:31:11');
INSERT INTO `log` VALUES ('26', '系统关闭之前回调beforeJFinalStop', '2016-05-05 22:33:40');
INSERT INTO `log` VALUES ('27', '系统启动之后回调afterJfinalStart', '2016-05-05 22:33:42');
INSERT INTO `log` VALUES ('28', '系统关闭之前回调beforeJFinalStop', '2016-05-05 22:33:56');
INSERT INTO `log` VALUES ('29', '系统启动之后回调afterJfinalStart', '2016-05-05 22:33:58');
INSERT INTO `log` VALUES ('30', '系统关闭之前回调beforeJFinalStop', '2016-05-05 22:47:39');
INSERT INTO `log` VALUES ('31', '系统启动之后回调afterJfinalStart', '2016-05-05 22:47:41');
INSERT INTO `log` VALUES ('32', '系统关闭之前回调beforeJFinalStop', '2016-05-05 22:59:31');
INSERT INTO `log` VALUES ('33', '系统启动之后回调afterJfinalStart', '2016-05-05 22:59:33');
INSERT INTO `log` VALUES ('34', '系统关闭之前回调beforeJFinalStop', '2016-05-05 22:59:51');
INSERT INTO `log` VALUES ('35', '系统启动之后回调afterJfinalStart', '2016-05-05 22:59:53');
INSERT INTO `log` VALUES ('36', '系统关闭之前回调beforeJFinalStop', '2016-05-05 23:09:13');
INSERT INTO `log` VALUES ('37', '系统启动之后回调afterJfinalStart', '2016-05-05 23:09:15');
INSERT INTO `log` VALUES ('38', '系统关闭之前回调beforeJFinalStop', '2016-05-05 23:15:12');
INSERT INTO `log` VALUES ('39', '系统启动之后回调afterJfinalStart', '2016-05-05 23:15:14');
INSERT INTO `log` VALUES ('40', '系统关闭之前回调beforeJFinalStop', '2016-05-05 23:15:22');
INSERT INTO `log` VALUES ('41', '系统启动之后回调afterJfinalStart', '2016-05-05 23:15:33');
INSERT INTO `log` VALUES ('42', '系统关闭之前回调beforeJFinalStop', '2016-05-05 23:25:12');
INSERT INTO `log` VALUES ('43', '系统启动之后回调afterJfinalStart', '2016-05-05 23:25:14');
INSERT INTO `log` VALUES ('44', '系统关闭之前回调beforeJFinalStop', '2016-05-05 23:25:27');
INSERT INTO `log` VALUES ('45', '系统启动之后回调afterJfinalStart', '2016-05-05 23:25:29');
INSERT INTO `log` VALUES ('46', '系统关闭之前回调beforeJFinalStop', '2016-05-05 23:25:37');
INSERT INTO `log` VALUES ('47', '系统启动之后回调afterJfinalStart', '2016-05-05 23:25:39');
INSERT INTO `log` VALUES ('48', '系统启动之后回调afterJfinalStart', '2016-05-12 14:48:13');
INSERT INTO `log` VALUES ('49', '系统关闭之前回调beforeJFinalStop', '2016-05-12 19:46:10');
INSERT INTO `log` VALUES ('50', '系统启动之后回调afterJfinalStart', '2016-05-12 19:46:13');
INSERT INTO `log` VALUES ('51', '系统启动之后回调afterJfinalStart', '2016-05-13 15:02:33');
INSERT INTO `log` VALUES ('52', '系统启动之后回调afterJfinalStart', '2016-05-13 15:04:55');
INSERT INTO `log` VALUES ('53', '系统启动之后回调afterJfinalStart', '2016-05-13 20:13:26');
INSERT INTO `log` VALUES ('54', '系统启动之后回调afterJfinalStart', '2016-05-13 20:29:20');
INSERT INTO `log` VALUES ('55', '系统启动之后回调afterJfinalStart', '2016-05-13 20:41:33');
INSERT INTO `log` VALUES ('56', '系统关闭之前回调beforeJFinalStop', '2016-05-13 20:47:34');
INSERT INTO `log` VALUES ('57', '系统启动之后回调afterJfinalStart', '2016-05-13 20:47:36');
INSERT INTO `log` VALUES ('58', '系统启动之后回调afterJfinalStart', '2016-05-13 20:50:31');
INSERT INTO `log` VALUES ('59', '系统启动之后回调afterJfinalStart', '2016-05-13 20:58:42');
INSERT INTO `log` VALUES ('60', '系统启动之后回调afterJfinalStart', '2016-05-13 21:18:22');
INSERT INTO `log` VALUES ('61', '系统关闭之前回调beforeJFinalStop', '2016-05-13 21:19:20');
INSERT INTO `log` VALUES ('62', '系统启动之后回调afterJfinalStart', '2016-05-13 21:19:22');
INSERT INTO `log` VALUES ('63', '系统关闭之前回调beforeJFinalStop', '2016-05-13 21:20:11');
INSERT INTO `log` VALUES ('64', '系统启动之后回调afterJfinalStart', '2016-05-13 21:20:13');
INSERT INTO `log` VALUES ('65', '系统关闭之前回调beforeJFinalStop', '2016-05-13 21:20:51');
INSERT INTO `log` VALUES ('66', '系统启动之后回调afterJfinalStart', '2016-05-13 21:20:53');
INSERT INTO `log` VALUES ('67', '系统关闭之前回调beforeJFinalStop', '2016-05-13 21:22:42');
INSERT INTO `log` VALUES ('68', '系统启动之后回调afterJfinalStart', '2016-05-13 21:22:44');
INSERT INTO `log` VALUES ('69', '系统启动之后回调afterJfinalStart', '2016-05-22 15:27:04');
INSERT INTO `log` VALUES ('70', '系统关闭之前回调beforeJFinalStop', '2016-05-22 15:27:17');
INSERT INTO `log` VALUES ('71', '系统启动之后回调afterJfinalStart', '2016-05-22 15:27:25');
INSERT INTO `log` VALUES ('72', '系统关闭之前回调beforeJFinalStop', '2016-05-22 15:59:47');
INSERT INTO `log` VALUES ('73', '系统启动之后回调afterJfinalStart', '2016-05-22 15:59:50');
INSERT INTO `log` VALUES ('74', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:00:48');
INSERT INTO `log` VALUES ('75', '系统启动之后回调afterJfinalStart', '2016-05-22 16:00:51');
INSERT INTO `log` VALUES ('76', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:01:13');
INSERT INTO `log` VALUES ('77', '系统启动之后回调afterJfinalStart', '2016-05-22 16:01:15');
INSERT INTO `log` VALUES ('78', '系统启动之后回调afterJfinalStart', '2016-05-22 16:07:09');
INSERT INTO `log` VALUES ('79', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:08:22');
INSERT INTO `log` VALUES ('80', '系统启动之后回调afterJfinalStart', '2016-05-22 16:08:24');
INSERT INTO `log` VALUES ('81', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:10:23');
INSERT INTO `log` VALUES ('82', '系统启动之后回调afterJfinalStart', '2016-05-22 16:10:31');
INSERT INTO `log` VALUES ('83', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:11:08');
INSERT INTO `log` VALUES ('84', '系统启动之后回调afterJfinalStart', '2016-05-22 16:11:10');
INSERT INTO `log` VALUES ('85', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:11:38');
INSERT INTO `log` VALUES ('86', '系统启动之后回调afterJfinalStart', '2016-05-22 16:11:40');
INSERT INTO `log` VALUES ('87', '系统关闭之前回调beforeJFinalStop', '2016-05-22 16:12:14');
INSERT INTO `log` VALUES ('88', '系统启动之后回调afterJfinalStart', '2016-05-22 16:12:15');
INSERT INTO `log` VALUES ('89', '系统启动之后回调afterJfinalStart', '2016-05-22 16:41:37');
INSERT INTO `log` VALUES ('90', '系统启动之后回调afterJfinalStart', '2016-05-22 19:24:13');
INSERT INTO `log` VALUES ('91', '系统关闭之前回调beforeJFinalStop', '2016-05-22 19:25:10');
INSERT INTO `log` VALUES ('92', '系统启动之后回调afterJfinalStart', '2016-05-22 19:25:13');
INSERT INTO `log` VALUES ('93', '系统关闭之前回调beforeJFinalStop', '2016-05-22 19:25:25');
INSERT INTO `log` VALUES ('94', '系统启动之后回调afterJfinalStart', '2016-05-22 19:25:27');
INSERT INTO `log` VALUES ('95', '系统关闭之前回调beforeJFinalStop', '2016-05-22 19:25:40');
INSERT INTO `log` VALUES ('96', '系统启动之后回调afterJfinalStart', '2016-05-22 19:26:02');
INSERT INTO `log` VALUES ('97', '系统启动之后回调afterJfinalStart', '2016-05-22 19:34:49');
INSERT INTO `log` VALUES ('98', '系统关闭之前回调beforeJFinalStop', '2016-05-22 19:36:38');
INSERT INTO `log` VALUES ('99', '系统启动之后回调afterJfinalStart', '2016-05-22 19:37:15');
INSERT INTO `log` VALUES ('100', '系统关闭之前回调beforeJFinalStop', '2016-05-22 19:40:45');
INSERT INTO `log` VALUES ('101', '系统启动之后回调afterJfinalStart', '2016-05-22 19:40:47');
INSERT INTO `log` VALUES ('102', '系统关闭之前回调beforeJFinalStop', '2016-05-22 19:52:32');
INSERT INTO `log` VALUES ('103', '系统启动之后回调afterJfinalStart', '2016-05-22 19:52:35');
INSERT INTO `log` VALUES ('104', '系统启动之后回调afterJfinalStart', '2016-05-23 21:11:49');
INSERT INTO `log` VALUES ('105', '系统关闭之前回调beforeJFinalStop', '2016-05-23 21:50:10');
INSERT INTO `log` VALUES ('106', '系统启动之后回调afterJfinalStart', '2016-05-23 21:50:12');
INSERT INTO `log` VALUES ('107', '系统启动之后回调afterJfinalStart', '2016-05-23 21:54:04');
INSERT INTO `log` VALUES ('108', '系统关闭之前回调beforeJFinalStop', '2016-05-23 21:55:01');
INSERT INTO `log` VALUES ('109', '系统启动之后回调afterJfinalStart', '2016-05-23 21:55:04');
INSERT INTO `log` VALUES ('110', '系统关闭之前回调beforeJFinalStop', '2016-05-23 21:55:21');
INSERT INTO `log` VALUES ('111', '系统启动之后回调afterJfinalStart', '2016-05-23 21:55:23');
INSERT INTO `log` VALUES ('112', '系统关闭之前回调beforeJFinalStop', '2016-05-23 21:56:52');
INSERT INTO `log` VALUES ('113', '系统启动之后回调afterJfinalStart', '2016-05-23 21:56:55');
INSERT INTO `log` VALUES ('114', '系统启动之后回调afterJfinalStart', '2016-05-23 21:58:38');
INSERT INTO `log` VALUES ('115', '系统关闭之前回调beforeJFinalStop', '2016-05-23 22:01:36');
INSERT INTO `log` VALUES ('116', '系统启动之后回调afterJfinalStart', '2016-05-23 22:01:39');
INSERT INTO `log` VALUES ('117', '系统启动之后回调afterJfinalStart', '2016-05-23 22:03:07');
INSERT INTO `log` VALUES ('118', '系统关闭之前回调beforeJFinalStop', '2016-05-23 22:04:35');
INSERT INTO `log` VALUES ('119', '系统启动之后回调afterJfinalStart', '2016-05-23 22:04:37');
INSERT INTO `log` VALUES ('120', '系统启动之后回调afterJfinalStart', '2016-05-23 22:08:32');
INSERT INTO `log` VALUES ('121', '系统启动之后回调afterJfinalStart', '2016-05-23 22:16:57');
INSERT INTO `log` VALUES ('122', '系统启动之后回调afterJfinalStart', '2016-05-23 22:18:49');
INSERT INTO `log` VALUES ('123', '系统关闭之前回调beforeJFinalStop', '2016-05-23 22:19:56');
INSERT INTO `log` VALUES ('124', '系统启动之后回调afterJfinalStart', '2016-05-23 22:19:59');
INSERT INTO `log` VALUES ('125', '系统启动之后回调afterJfinalStart', '2016-05-24 21:20:19');
INSERT INTO `log` VALUES ('126', '系统启动之后回调afterJfinalStart', '2016-05-24 21:50:53');
INSERT INTO `log` VALUES ('127', '系统启动之后回调afterJfinalStart', '2016-05-24 21:55:58');
INSERT INTO `log` VALUES ('128', '系统关闭之前回调beforeJFinalStop', '2016-05-24 21:57:41');
INSERT INTO `log` VALUES ('129', '系统启动之后回调afterJfinalStart', '2016-05-24 21:57:43');
INSERT INTO `log` VALUES ('130', '系统关闭之前回调beforeJFinalStop', '2016-05-24 22:12:50');
INSERT INTO `log` VALUES ('131', '系统启动之后回调afterJfinalStart', '2016-05-24 22:12:53');
INSERT INTO `log` VALUES ('132', '系统启动之后回调afterJfinalStart', '2016-05-24 22:13:41');
INSERT INTO `log` VALUES ('133', '系统关闭之前回调beforeJFinalStop', '2016-05-24 22:18:11');
INSERT INTO `log` VALUES ('134', '系统启动之后回调afterJfinalStart', '2016-05-24 22:18:14');
INSERT INTO `log` VALUES ('135', '系统启动之后回调afterJfinalStart', '2016-05-24 22:18:44');
INSERT INTO `log` VALUES ('136', '系统启动之后回调afterJfinalStart', '2016-05-24 22:26:25');
INSERT INTO `log` VALUES ('137', '系统启动之后回调afterJfinalStart', '2016-05-24 22:35:51');
INSERT INTO `log` VALUES ('138', '系统启动之后回调afterJfinalStart', '2016-05-27 21:56:04');
INSERT INTO `log` VALUES ('139', '系统启动之后回调afterJfinalStart', '2016-06-01 10:07:17');
INSERT INTO `log` VALUES ('140', '系统启动之后回调afterJfinalStart', '2016-06-01 10:38:50');
INSERT INTO `log` VALUES ('141', '系统关闭之前回调beforeJFinalStop', '2016-06-01 10:49:49');
INSERT INTO `log` VALUES ('142', '系统启动之后回调afterJfinalStart', '2016-06-01 10:49:51');
INSERT INTO `log` VALUES ('143', '系统关闭之前回调beforeJFinalStop', '2016-06-01 10:51:35');
INSERT INTO `log` VALUES ('144', '系统启动之后回调afterJfinalStart', '2016-06-01 10:51:37');
INSERT INTO `log` VALUES ('145', '系统关闭之前回调beforeJFinalStop', '2016-06-01 10:52:56');
INSERT INTO `log` VALUES ('146', '系统启动之后回调afterJfinalStart', '2016-06-01 10:52:57');
INSERT INTO `log` VALUES ('147', '系统启动之后回调afterJfinalStart', '2016-06-01 10:53:38');
INSERT INTO `log` VALUES ('148', '系统关闭之前回调beforeJFinalStop', '2016-06-01 10:54:16');
INSERT INTO `log` VALUES ('149', '系统启动之后回调afterJfinalStart', '2016-06-01 10:54:19');
INSERT INTO `log` VALUES ('150', '系统关闭之前回调beforeJFinalStop', '2016-06-01 10:57:13');
INSERT INTO `log` VALUES ('151', '系统启动之后回调afterJfinalStart', '2016-06-01 10:57:14');
INSERT INTO `log` VALUES ('152', '系统关闭之前回调beforeJFinalStop', '2016-06-01 10:57:28');
INSERT INTO `log` VALUES ('153', '系统启动之后回调afterJfinalStart', '2016-06-01 10:57:29');
INSERT INTO `log` VALUES ('154', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:00:30');
INSERT INTO `log` VALUES ('155', '系统启动之后回调afterJfinalStart', '2016-06-01 11:00:31');
INSERT INTO `log` VALUES ('156', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:03:57');
INSERT INTO `log` VALUES ('157', '系统启动之后回调afterJfinalStart', '2016-06-01 11:03:58');
INSERT INTO `log` VALUES ('158', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:05:23');
INSERT INTO `log` VALUES ('159', '系统启动之后回调afterJfinalStart', '2016-06-01 11:05:24');
INSERT INTO `log` VALUES ('160', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:05:48');
INSERT INTO `log` VALUES ('161', '系统启动之后回调afterJfinalStart', '2016-06-01 11:05:50');
INSERT INTO `log` VALUES ('162', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:06:18');
INSERT INTO `log` VALUES ('163', '系统启动之后回调afterJfinalStart', '2016-06-01 11:06:20');
INSERT INTO `log` VALUES ('164', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:07:04');
INSERT INTO `log` VALUES ('165', '系统启动之后回调afterJfinalStart', '2016-06-01 11:07:05');
INSERT INTO `log` VALUES ('166', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:07:34');
INSERT INTO `log` VALUES ('167', '系统启动之后回调afterJfinalStart', '2016-06-01 11:07:36');
INSERT INTO `log` VALUES ('168', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:08:24');
INSERT INTO `log` VALUES ('169', '系统启动之后回调afterJfinalStart', '2016-06-01 11:08:26');
INSERT INTO `log` VALUES ('170', '系统启动之后回调afterJfinalStart', '2016-06-01 11:12:00');
INSERT INTO `log` VALUES ('171', '系统关闭之前回调beforeJFinalStop', '2016-06-01 11:16:25');
INSERT INTO `log` VALUES ('172', '系统启动之后回调afterJfinalStart', '2016-06-01 11:16:28');
INSERT INTO `log` VALUES ('173', '系统启动之后回调afterJfinalStart', '2016-06-01 11:47:51');
INSERT INTO `log` VALUES ('174', '系统启动之后回调afterJfinalStart', '2016-06-02 21:15:17');
INSERT INTO `log` VALUES ('175', '系统关闭之前回调beforeJFinalStop', '2016-06-02 21:37:18');
INSERT INTO `log` VALUES ('176', '系统启动之后回调afterJfinalStart', '2016-06-02 21:37:19');
INSERT INTO `log` VALUES ('177', '系统关闭之前回调beforeJFinalStop', '2016-06-02 21:38:24');
INSERT INTO `log` VALUES ('178', '系统启动之后回调afterJfinalStart', '2016-06-02 21:38:26');
INSERT INTO `log` VALUES ('179', '系统启动之后回调afterJfinalStart', '2016-06-02 21:45:02');
INSERT INTO `log` VALUES ('180', 'ϵͳ�ر�֮ǰ�ص�beforeJFinalStop', '2016-06-02 21:51:58');
INSERT INTO `log` VALUES ('181', 'ϵͳ����֮��ص�afterJfinalStart', '2016-06-02 21:52:00');
INSERT INTO `log` VALUES ('182', '系统启动之后回调afterJfinalStart', '2016-06-02 22:06:08');
INSERT INTO `log` VALUES ('183', '系统关闭之前回调beforeJFinalStop', '2016-06-02 22:08:10');
INSERT INTO `log` VALUES ('184', '系统启动之后回调afterJfinalStart', '2016-06-02 22:08:14');
INSERT INTO `log` VALUES ('185', '系统关闭之前回调beforeJFinalStop', '2016-06-02 22:08:41');
INSERT INTO `log` VALUES ('186', '系统启动之后回调afterJfinalStart', '2016-06-02 22:08:43');
INSERT INTO `log` VALUES ('187', '系统启动之后回调afterJfinalStart', '2016-06-02 22:28:51');
INSERT INTO `log` VALUES ('188', '系统关闭之前回调beforeJFinalStop', '2016-06-02 22:29:39');
INSERT INTO `log` VALUES ('189', '系统启动之后回调afterJfinalStart', '2016-06-02 22:29:41');
INSERT INTO `log` VALUES ('190', '系统关闭之前回调beforeJFinalStop', '2016-06-02 22:30:50');
INSERT INTO `log` VALUES ('191', '系统启动之后回调afterJfinalStart', '2016-06-02 22:30:53');
INSERT INTO `log` VALUES ('192', '系统关闭之前回调beforeJFinalStop', '2016-06-02 22:31:05');
INSERT INTO `log` VALUES ('193', '系统启动之后回调afterJfinalStart', '2016-06-02 22:31:06');
INSERT INTO `log` VALUES ('194', '系统启动之后回调afterJfinalStart', '2016-06-03 09:48:50');
INSERT INTO `log` VALUES ('195', '系统启动之后回调afterJfinalStart', '2016-06-03 09:58:37');
INSERT INTO `log` VALUES ('196', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:07:45');
INSERT INTO `log` VALUES ('197', '系统启动之后回调afterJfinalStart', '2016-06-03 10:07:47');
INSERT INTO `log` VALUES ('198', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:09:00');
INSERT INTO `log` VALUES ('199', '系统启动之后回调afterJfinalStart', '2016-06-03 10:09:03');
INSERT INTO `log` VALUES ('200', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:09:36');
INSERT INTO `log` VALUES ('201', '系统启动之后回调afterJfinalStart', '2016-06-03 10:09:38');
INSERT INTO `log` VALUES ('202', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:10:26');
INSERT INTO `log` VALUES ('203', '系统启动之后回调afterJfinalStart', '2016-06-03 10:10:28');
INSERT INTO `log` VALUES ('204', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:10:36');
INSERT INTO `log` VALUES ('205', '系统启动之后回调afterJfinalStart', '2016-06-03 10:10:38');
INSERT INTO `log` VALUES ('206', '系统启动之后回调afterJfinalStart', '2016-06-03 10:16:10');
INSERT INTO `log` VALUES ('207', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:23:37');
INSERT INTO `log` VALUES ('208', '系统启动之后回调afterJfinalStart', '2016-06-03 10:23:40');
INSERT INTO `log` VALUES ('209', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:23:57');
INSERT INTO `log` VALUES ('210', '系统启动之后回调afterJfinalStart', '2016-06-03 10:23:59');
INSERT INTO `log` VALUES ('211', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:24:28');
INSERT INTO `log` VALUES ('212', '系统启动之后回调afterJfinalStart', '2016-06-03 10:24:30');
INSERT INTO `log` VALUES ('213', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:28:15');
INSERT INTO `log` VALUES ('214', '系统启动之后回调afterJfinalStart', '2016-06-03 10:28:18');
INSERT INTO `log` VALUES ('215', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:28:30');
INSERT INTO `log` VALUES ('216', '系统启动之后回调afterJfinalStart', '2016-06-03 10:28:32');
INSERT INTO `log` VALUES ('217', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:29:51');
INSERT INTO `log` VALUES ('218', '系统启动之后回调afterJfinalStart', '2016-06-03 10:29:54');
INSERT INTO `log` VALUES ('219', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:32:58');
INSERT INTO `log` VALUES ('220', '系统启动之后回调afterJfinalStart', '2016-06-03 10:33:00');
INSERT INTO `log` VALUES ('221', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:34:29');
INSERT INTO `log` VALUES ('222', '系统启动之后回调afterJfinalStart', '2016-06-03 10:34:31');
INSERT INTO `log` VALUES ('223', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:38:01');
INSERT INTO `log` VALUES ('224', '系统启动之后回调afterJfinalStart', '2016-06-03 10:38:03');
INSERT INTO `log` VALUES ('225', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:42:24');
INSERT INTO `log` VALUES ('226', '系统启动之后回调afterJfinalStart', '2016-06-03 10:42:26');
INSERT INTO `log` VALUES ('227', '系统关闭之前回调beforeJFinalStop', '2016-06-03 10:46:16');
INSERT INTO `log` VALUES ('228', '系统启动之后回调afterJfinalStart', '2016-06-03 10:46:19');
INSERT INTO `log` VALUES ('229', '系统启动之后回调afterJfinalStart', '2016-06-03 17:24:31');
INSERT INTO `log` VALUES ('230', '系统启动之后回调afterJfinalStart', '2016-06-03 17:39:16');
INSERT INTO `log` VALUES ('231', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:44:46');
INSERT INTO `log` VALUES ('232', '系统启动之后回调afterJfinalStart', '2016-06-03 17:44:49');
INSERT INTO `log` VALUES ('233', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:47:38');
INSERT INTO `log` VALUES ('234', '系统启动之后回调afterJfinalStart', '2016-06-03 17:47:43');
INSERT INTO `log` VALUES ('235', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:48:14');
INSERT INTO `log` VALUES ('236', '系统启动之后回调afterJfinalStart', '2016-06-03 17:48:16');
INSERT INTO `log` VALUES ('237', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:48:44');
INSERT INTO `log` VALUES ('238', '系统启动之后回调afterJfinalStart', '2016-06-03 17:48:47');
INSERT INTO `log` VALUES ('239', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:49:09');
INSERT INTO `log` VALUES ('240', '系统启动之后回调afterJfinalStart', '2016-06-03 17:49:12');
INSERT INTO `log` VALUES ('241', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:50:20');
INSERT INTO `log` VALUES ('242', '系统启动之后回调afterJfinalStart', '2016-06-03 17:50:22');
INSERT INTO `log` VALUES ('243', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:50:30');
INSERT INTO `log` VALUES ('244', '系统启动之后回调afterJfinalStart', '2016-06-03 17:50:33');
INSERT INTO `log` VALUES ('245', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:51:41');
INSERT INTO `log` VALUES ('246', '系统启动之后回调afterJfinalStart', '2016-06-03 17:53:37');
INSERT INTO `log` VALUES ('247', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:54:34');
INSERT INTO `log` VALUES ('248', '系统启动之后回调afterJfinalStart', '2016-06-03 17:54:37');
INSERT INTO `log` VALUES ('249', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:55:25');
INSERT INTO `log` VALUES ('250', '系统启动之后回调afterJfinalStart', '2016-06-03 17:55:27');
INSERT INTO `log` VALUES ('251', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:55:55');
INSERT INTO `log` VALUES ('252', '系统启动之后回调afterJfinalStart', '2016-06-03 17:55:57');
INSERT INTO `log` VALUES ('253', '系统关闭之前回调beforeJFinalStop', '2016-06-03 17:56:51');
INSERT INTO `log` VALUES ('254', '系统启动之后回调afterJfinalStart', '2016-06-03 17:56:53');
INSERT INTO `log` VALUES ('255', '系统关闭之前回调beforeJFinalStop', '2016-06-03 18:01:59');
INSERT INTO `log` VALUES ('256', '系统启动之后回调afterJfinalStart', '2016-06-03 18:02:01');
INSERT INTO `log` VALUES ('257', '系统关闭之前回调beforeJFinalStop', '2016-06-03 18:02:49');
INSERT INTO `log` VALUES ('258', '系统启动之后回调afterJfinalStart', '2016-06-03 18:02:52');
INSERT INTO `log` VALUES ('259', '系统关闭之前回调beforeJFinalStop', '2016-06-03 18:03:40');
INSERT INTO `log` VALUES ('260', '系统启动之后回调afterJfinalStart', '2016-06-03 18:03:43');
INSERT INTO `log` VALUES ('261', '系统关闭之前回调beforeJFinalStop', '2016-06-03 18:04:10');
INSERT INTO `log` VALUES ('262', '系统启动之后回调afterJfinalStart', '2016-06-03 18:04:12');
INSERT INTO `log` VALUES ('263', '系统启动之后回调afterJfinalStart', '2016-06-14 09:58:34');
INSERT INTO `log` VALUES ('264', '系统关闭之前回调beforeJFinalStop', '2016-06-14 10:12:13');
INSERT INTO `log` VALUES ('265', '系统启动之后回调afterJfinalStart', '2016-06-14 10:12:16');
INSERT INTO `log` VALUES ('266', '系统关闭之前回调beforeJFinalStop', '2016-06-14 10:56:20');
INSERT INTO `log` VALUES ('267', '系统启动之后回调afterJfinalStart', '2016-06-14 10:56:22');
INSERT INTO `log` VALUES ('268', '系统启动之后回调afterJfinalStart', '2016-06-14 12:28:09');
INSERT INTO `log` VALUES ('269', '系统关闭之前回调beforeJFinalStop', '2016-06-14 12:29:07');
INSERT INTO `log` VALUES ('270', '系统启动之后回调afterJfinalStart', '2016-06-14 12:29:10');
INSERT INTO `log` VALUES ('271', '系统启动之后回调afterJfinalStart', '2016-06-14 12:31:26');
INSERT INTO `log` VALUES ('272', '系统关闭之前回调beforeJFinalStop', '2016-06-14 12:32:59');
INSERT INTO `log` VALUES ('273', '系统启动之后回调afterJfinalStart', '2016-06-14 12:33:00');
INSERT INTO `log` VALUES ('274', '系统启动之后回调afterJfinalStart', '2016-06-14 12:37:48');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `remark` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('5', '小木2016', '27', '1', '小木学堂讲师');
INSERT INTO `user` VALUES ('6', '小木学堂', '28', '1', '小木学堂讲师 jfinal课程');
