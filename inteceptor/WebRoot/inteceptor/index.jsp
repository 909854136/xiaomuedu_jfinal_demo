<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="../common/header.jsp"></jsp:include>
 <style>
  .row{margin-bottom: 10px;}
  </style>
<script src="static/js/ajax.js"></script>
  <header class="navbar navbar-static-top" id="top" role="banner">
  <div class="container">
    <div class="navbar-header" style="text-align: center;">
      <a href="/" class="navbar-brand">小木学堂JFinal课程案例0017-极速AOP&拦截器专题</a>
    </div>
  </div>
 
</header>
<div id="main" class="container"  style="font-size:18px;">
  <div class="row">
  <div class="col-lg-6 col-sm-6 col-md-6">
	<div class="panel panel-default">
  <div class="panel-heading">极速AOP&拦截器</div>
  <div class="panel-body">
	<li class="list-group-item"><a class="ajax" href="inteceptor/globalaction">1、全局拦截器-action</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/globalservice">2、全局拦截器-service</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/classinc">3、class拦截器</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/methodinc">4、method拦截器</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/injectinc">5、Inject拦截器</a></li>
	<li class="list-group-item"><a>==权限小专题==</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/addPer">6、增加权限</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/removePer">7、去掉权限</a></li>
	<li class="list-group-item"><a class="ajax" href="inteceptor/finance">8、权限控制测试</a></li>
  </div>
</div>
</div>
  <div class="col-lg-6 col-sm-6 col-md-6" >
	<div class="panel panel-default pinned" style="margin-top: 10px;">
  <div class="panel-heading">控制台</div>
  <div class="panel-body">
  <div id="Canvas" class="Canvas"></div>
  </div>
</div>
  </div>
  </div>  
</div>
<script type="text/javascript">
$(function(){
	$("li a.ajax").on("click",function(){
		var _this=$(this);
		var url=_this.attr("href");
		appLoading("执行中...");
		$.ajax({
			type:"get",
			url:url,
			dataType:"json",
			success:function(data){
				closeAppLoading();
				JsonConsoleProcess(data);
			}
		});
		return false;
	});
});

</script>
<jsp:include page="../common/footer.jsp"></jsp:include>
