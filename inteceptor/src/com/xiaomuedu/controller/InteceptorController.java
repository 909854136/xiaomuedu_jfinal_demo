package com.xiaomuedu.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.xiaomuedu.auth.FuncPermission;
import com.xiaomuedu.auth.Permission;
import com.xiaomuedu.inteceptor.ClassInteceptor;
import com.xiaomuedu.inteceptor.ClassTwoInteceptor;
import com.xiaomuedu.inteceptor.MethodInteceptor;
import com.xiaomuedu.service.BaseService;
@Before({ClassInteceptor.class,ClassTwoInteceptor.class})
public class InteceptorController extends Controller {
	public void index(){
		System.out.println("InteceptorController-index被调用");
		renderJsp("index.jsp");
	}
	/**
	 * 全局action拦截器测试
	 */
	public void globalaction(){
		setAttr("msg", "InteceptorController-globalaction");
		renderJson();
	}
	public void addPer(){
		setSessionAttr("funcpermission", FuncPermission.FINANCE_MGR);
		setAttr("result", "成功的访问InteceptorController-addPer");
		renderJson();
	}
	public void removePer(){
		removeSessionAttr("funcpermission");
		setAttr("result", "成功的访问InteceptorController-removePer");
		renderJson();
	}
	@Permission(FuncPermission.FINANCE_MGR)
	public void finance(){
		setAttr("result", "成功的访问InteceptorController-finance");
		renderJson();
	}
	
	public void classinc(){
		setAttr("msg", "成功的访问InteceptorController-classinc");
		renderJson();
	}
	
	@Before(MethodInteceptor.class)
	public void methodinc(){
		setAttr("msg", "成功的访问InteceptorController-methodinc");
		renderJson();
	}
	/**
	 * 全局service级别拦截器
	 */
	public void globalservice(){
		boolean result=BaseService.me.doSomthing();
		setAttr("msg", "成功的访问InteceptorController-globalservice:"+result);
		renderJson();
	}
}
