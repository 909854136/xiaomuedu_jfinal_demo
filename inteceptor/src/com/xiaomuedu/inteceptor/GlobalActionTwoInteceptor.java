package com.xiaomuedu.inteceptor;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

public class GlobalActionTwoInteceptor implements  Interceptor {
	/**
	 * 判断当前请求是ajax请求
	 * @return
	 */
	public boolean isAjax(Controller controller){
		  HttpServletRequest request=controller.getRequest();
		  String requestedWith=request.getHeader("x-requested-with");
		  return StrKit.notBlank(requestedWith)&&requestedWith.equals("XMLHttpRequest");
	}
	
	@Override
	public void intercept(Invocation inv) {
		System.out.println("GlobalActionTwoInteceptor 前拦截");
		Controller controller=inv.getController();
		controller.setAttr("msg1", "GlobalActionTwoInteceptor-前拦截");
		controller.setAttr("isAjax", isAjax(controller));
		System.out.println("GlobalActionTwoInteceptor-isAjax="+isAjax(controller));
		inv.invoke();
		controller.setAttr("msg2", "GlobalActionTwoInteceptor-后拦截");
		System.out.println("GlobalActionTwoInteceptor 后拦截");
		
	}

}
