package com.xiaomuedu.inteceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
/**
 * service级别
 * @author Administrator
 *
 */
public class GlobalServiceInteceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("调用Globalservice级别拦截器:"+inv.isActionInvocation());
		inv.invoke();
	}

}
