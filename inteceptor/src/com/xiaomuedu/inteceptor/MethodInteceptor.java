package com.xiaomuedu.inteceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
/**
 * method级别拦截器
 * @author Administrator
 *
 */
public class MethodInteceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("调用Method级别拦截器 MethodInteceptor");
		inv.getController().setAttr("methodinv", "调用Method级别拦截器 MethodInteceptor");
		inv.invoke();
	}

}
