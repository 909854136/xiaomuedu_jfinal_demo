package com.xiaomuedu.inteceptor;

import java.lang.reflect.Method;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.xiaomuedu.auth.FuncPermission;
import com.xiaomuedu.auth.Permission;

public class GlobalActionOneInteceptor implements  Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("GlobalActionOneInteceptor 前拦截");
		boolean hasAtuh=true;
		Controller controller=inv.getController();
		Method method=inv.getMethod();
		if(method.isAnnotationPresent(Permission.class)){
			hasAtuh=false;
			Object obj= controller.getSessionAttr("funcpermission");
			if(obj==null){
				controller.setAttr("errormsg", "无权访问");
				controller.renderJson();
				return;
			}
			int user_funcper=Integer.parseInt(obj.toString());
			//配置类权限拦截
			Permission permission=method.getAnnotation(Permission.class);
			int[] funpers=permission.value();
			for(int funcper:funpers){
				if(funcper==user_funcper){
					hasAtuh=true;
					break;
				}
			}
		}
		if(hasAtuh){
			controller.setAttr("hasAuth", "有权访问");
			inv.invoke();
		}
		System.out.println("GlobalActionOneInteceptor 后拦截");
		
	}

}
