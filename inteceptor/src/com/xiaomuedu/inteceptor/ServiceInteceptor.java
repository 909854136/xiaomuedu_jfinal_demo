package com.xiaomuedu.inteceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class ServiceInteceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("调用service级别拦截器:"+inv.isActionInvocation());
		inv.invoke();

	}

}
