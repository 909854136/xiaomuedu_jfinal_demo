package com.xiaomuedu.inteceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
/**
 * 类级别拦截器
 * @author Administrator
 *
 */
public class ClassTwoInteceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("调用class级别拦截器 ClassTwoInteceptor");
		inv.getController().setAttr("classtwoinv", "调用class级别拦截器 ClassTwoInteceptor");
		inv.invoke();
	}

}
