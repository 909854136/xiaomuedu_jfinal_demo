package com.xiaomuedu.service;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Enhancer;
import com.xiaomuedu.inteceptor.ServiceInteceptor;

public class BaseService {
	public static final BaseService me=Enhancer.enhance(BaseService.class);
	@Clear
	@Before(ServiceInteceptor.class)
	public boolean doSomthing(){
		//处理业务逻辑
		System.out.println("调用了BaseService doSomthing");
		return true;
	}
}
