package com.xiaomuedu.controller;

import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.jfinal.plugin.ehcache.CacheName;
import com.xiaomuedu.common.model.Employee;
/**
 * 员工管理
 * @author 小木
 *
 */
public class EmployeeController extends Controller {
	public void index(){
		renderJsp("index.jsp");
	}
	/**
	 * 普通列表查询
	 */
	@Before(CacheInterceptor.class)
	@CacheName("xiaomuedu")
	public void normal(){
		List<Employee> es=Employee.dao.find("select * from employee");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-normal");
		renderJson();
	}
	/**
	 * left join
	 */
	public void leftjoin(){
		List<Employee> es=Employee.dao.find("select e.*,d.name as deptName from employee e left join dept d on e.dept_id=d.id");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-leftjoin");
		renderJson();
	}
	/**
	 * right join
	 */
	public void rightjoin(){
		List<Employee> es=Employee.dao.find("select e.*,d.* from employee e right join dept d on e.dept_id=d.id");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-rightjoin");
		renderJson();
	}
	/**
	 * inner join
	 */
	public void innerjoin(){
		List<Employee> es=Employee.dao.find("select e.*,d.name as deptName from employee e inner join dept d on e.dept_id=d.id");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-innerjoin");
		renderJson();
	}
	/**
	 * full join
	 */
	public void fulljoin(){
		List<Employee> es=Employee.dao.find("select * from employee e left join dept d on e.dept_id=d.id  union select * from employee e right join dept d on e.dept_id=d.id");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-fulljoin");
		renderJson();
	}
	/**
	 * or
	 */
	public void or(){
		List<Employee> es=Employee.dao.find("select * from employee where dept_id=1 or dept_id=2");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-or");
		renderJson();
	}
	/**
	 * union join
	 */
	public void unionjoin(){
		List<Employee> es=Employee.dao.find("select * from employee where dept_id=1 union select * from employee where dept_id=2");
		setAttr("datas", es);
		setAttr("msg", "调用EmployeController-unionjoin");
		renderJson();
	}
	/**
	 * model join
	 */
	public void modeljoin(){
		List<Employee> es=Employee.dao.find("select * from employee");
		setAttr("datas", es);
		
		setAttr("msg", "调用EmployeController-modeljoin");
		renderJson();
	}
}
