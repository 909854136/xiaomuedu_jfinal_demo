/**
 * 小木学堂 公开课代码第一季第001期 autoLoad select
 */
var AutoSelectUtil={
		fillIt:function(select,datas){
			if(datas&&datas.length>0){
				for(var i in datas){
					select.append('<option value="'+datas[i].optionValue+'">'+datas[i].optionText+'</option>');
				}
			}
		},
		load:function(select){
			var url=select.data("url");
			$.ajax({
				type:"get",
				dateType:"json",
				url:url,
				success:function(data){
					if(data.success){
						AutoSelectUtil.fillIt(select,data.result);
					}
				}
			})
		},
		autoload:function(){
			$(".autoloadSelect").each(function(){
				AutoSelectUtil.load($(this)); 
			});
		}
}


$(function(){
	AutoSelectUtil.autoload();
});