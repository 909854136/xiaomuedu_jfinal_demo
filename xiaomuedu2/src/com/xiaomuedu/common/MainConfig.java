package com.xiaomuedu.common;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.AnsiSqlDialect;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.xiaomuedu.common.model.Dept;
import com.xiaomuedu.common.model.Employee;
import com.xiaomuedu.common.model.User;
import com.xiaomuedu.controller.EmployeeController;
import com.xiaomuedu.controller.UserController;

public class MainConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		//JFinal常量配置讲解
		//常量配置是第一个被加载的因为后面的一些配置都需要这里配置的常量作为基础
		// 1.加载数据库配置 读取配置文件使用loadPropertyFile或者PropKit  读取键值对
		//loadPropertyFile("config.properties");
		PropKit.use("config.properties");
		// 2、设置开发模式
		me.setDevMode(PropKit.getBoolean("devMode"));
//		me.setDevMode(getPropertyToBoolean("devMode"));
		//设置Action Report什么出现 默认true
		me.setReportAfterInvocation(false);
		// 3、配置默认的视图类型 默认是Freemarker
		me.setViewType(ViewType.JSP);
		// 4、配置默认视图层路径viewpath
		//me.setBaseViewPath("/WEB-INF/view");
		// 5、设置默认上传路径 cos组件有效 jfinal默认有值 相对 绝对都可以
		me.setBaseUploadPath("xmupload");
		//me.setMaxPostSize(1024*1024*20);
		// 6、设置默认下载路径 cos组件有效 jfinal默认有值  相对 绝对都可以
		me.setBaseDownloadPath("xmdownload");
		// 7、设置默认的Freemarker模板文件后缀名 jfinal默认.html
		//me.setFreeMarkerViewExtension(".ftl");
		//me.setJspViewExtension(".jtl");
		//me.setVelocityViewExtension(".vtl");
		// 8、这是url参数分隔符  默认-
		//me.setUrlParaSeparator("~");
		//设置国际化
		//me.setI18nDefaultBaseName("i18n");
		//me.setI18nDefaultLocale("zh_CN"");
		//设置Error View
		//me.setError404View("/common/404.html");
		//me.setErrorRenderFactory(errorRenderFactory);
		//设置默认编码
		//me.setEncoding("GBK");
		//设置默认的xml渲染工厂 默认使用Freemarker render渲染
		//me.setXmlRenderFactory(自定义工厂);
		//设置默认json中时间格式化
		//me.setJsonDatePattern("yyyy-mm-dd HH:mm");
		//me.setJsonFactory(FastJsonFactory.me());
		//renderJson 和JsonKit底层依赖于JsonManager中设置的JsonFactory
		//设置自己的Log工厂实现
		//me.setLogFactory(Slf4JLogFactory.me());
		
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/user", UserController.class);
		me.add("/employee", EmployeeController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		//Jfinal配置插件
		//sqlite
		//数据库连接池
		//读写分离模拟
	/*	C3p0Plugin sqliteC3p0Plugin= new C3p0Plugin(PropKit.get("sqlite_jdbcUrl"), "", "");
		sqliteC3p0Plugin.setDriverClass("org.sqlite.JDBC");
		//ORM Activerecord
		ActiveRecordPlugin sqlitearp = new ActiveRecordPlugin("writeServer",sqliteC3p0Plugin);
		sqlitearp.setShowSql(true);
		sqlitearp.setDialect(new Sqlite3Dialect());
		_MappingKit.mapping(sqlitearp);
		
		//h2
		//数据库连接池
		C3p0Plugin h2c3p0Plugin = new C3p0Plugin(PropKit.get("h2_jdbcurl"),  PropKit.get("user"),  PropKit.get("password"));
		h2c3p0Plugin.setDriverClass("org.h2.Driver");
		//ORM Activerecord
		ActiveRecordPlugin h2arp = new ActiveRecordPlugin("readServer",h2c3p0Plugin);
		h2arp.setShowSql(true);
		h2arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		h2arp.setDialect(new AnsiSqlDialect());
		_MappingKit.mapping(h2arp);*/
		
		
		//表分布在不同数据库上 数据源上
			C3p0Plugin sqliteC3p0Plugin= new C3p0Plugin(PropKit.get("sqlite_jdbcUrl"), "", "");
		sqliteC3p0Plugin.setDriverClass("org.sqlite.JDBC");
		//ORM Activerecord
		ActiveRecordPlugin sqlitearp = new ActiveRecordPlugin("sqlite",sqliteC3p0Plugin);
		sqlitearp.setShowSql(true);
		sqlitearp.setDialect(new Sqlite3Dialect());
		sqlitearp.addMapping("employee", Employee.class);
		sqlitearp.addMapping("dept", Dept.class);
		
		//h2
		//数据库连接池
		C3p0Plugin h2c3p0Plugin = new C3p0Plugin(PropKit.get("h2_jdbcurl"),  PropKit.get("user"),  PropKit.get("password"));
		h2c3p0Plugin.setDriverClass("org.h2.Driver");
		//ORM Activerecord
		ActiveRecordPlugin h2arp = new ActiveRecordPlugin("h2",h2c3p0Plugin);
		h2arp.setShowSql(true);
		h2arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		h2arp.setDialect(new AnsiSqlDialect());
		h2arp.addMapping("user", User.class);
		
		//加入插件管理器
		
		me.add(sqliteC3p0Plugin);
		me.add(sqlitearp);
		
		me.add(h2c3p0Plugin);
		me.add(h2arp);
	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configHandler(Handlers me) {
	}
	public static void main(String[] args) {
		JFinal.start("WebRoot", 80, "/", 5);
	}

}
